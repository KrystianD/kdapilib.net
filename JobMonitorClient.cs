﻿using System.Threading.Tasks;
using KDLib;
using Newtonsoft.Json.Linq;
using RequestsNET;

namespace KDAPILib.JobMonitor
{
  public class JobMonitorClient
  {
    public Task CreateJob(string appId, string jobName, int interval, int graceTime, int? notificationInterval = null)
    {
      return Requests.PostAsync($"https://jobmonitor.kdapps.net/api/apps/{appId}/jobs/{jobName}",
                                json: JToken.FromObject(new {
                                    interval = interval,
                                    grace_time = graceTime,
                                    notification_interval = notificationInterval,
                                }));
    }

    public JobMonitorApplicationClient GetApplicationClient(string appId)
    {
      return new JobMonitorApplicationClient(this, appId);
    }
  }

  public class JobMonitorApplicationClient
  {
    private readonly JobMonitorClient _client;
    private readonly string _appId;

    public JobMonitorApplicationClient(JobMonitorClient client, string appId)
    {
      _client = client;
      _appId = appId;
    }

    public void CreateJob(string jobName, int interval, int graceTime, int? notificationInterval = null)
    {
      Task.Run(() => _client.CreateJob(_appId, jobName, interval, graceTime, notificationInterval));
    }
  }
}