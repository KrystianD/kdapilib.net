using Newtonsoft.Json.Linq;

namespace KDAPILib.Builders
{
  public interface IFeedItem
  {
    JObject Build();
  }

  public static class FeedItemBuilder
  {
    private class FeedItem : IFeedItem
    {
      private readonly JObject _data;

      public FeedItem(JObject data)
      {
        _data = data;
      }

      public JObject Build() => _data;
    }

    public static IFeedItem TextPost(string text)
    {
      return new FeedItem(JObject.FromObject(new {
          Type = "TextPost",
          Body = text,
      }));
    }

    public static IFeedItem LinkPost(string text, string link)
    {
      return new FeedItem(JObject.FromObject(new {
          Type = "LinkPost",
          Body = text,
          LinkUrl = link,
      }));
    }
  }
}