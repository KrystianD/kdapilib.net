﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using KDLib;
using KDLib.JsonConverters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RequestsNET;

namespace KDAPILib.TypeForm
{
  public class TypeFormAPI
  {
    public class TypeFormResponse
    {
      public class MetadataClass
      {
        [JsonProperty("browser", Required = Required.Always)]
        public string Browser;

        [JsonProperty("platform", Required = Required.Always)]
        public string Platform;

        [JsonProperty("user_agent", Required = Required.Always)]
        public string UserAgent;

        [JsonProperty("referer", Required = Required.Always)]
        public string Referer;

        [JsonProperty("network_id", Required = Required.Always)]
        public string NetworkId;
      }

      public class FieldDescriptorClass
      {
        [SuppressMessage("ReSharper", "UnusedMember.Global")]
        [JsonConverter(typeof(StringEnumConverter))]
        public enum FieldTypeEnum
        {
          [EnumMember(Value = "rating")]
          Rating,

          [EnumMember(Value = "short_text")]
          ShortText,

          [EnumMember(Value = "long_text")]
          LongText,

          [EnumMember(Value = "email")]
          Email,

          [EnumMember(Value = "phone_number")]
          PhoneNumber,

          [EnumMember(Value = "opinion_scale")]
          OpinionScale,

          [EnumMember(Value = "picture_choice")]
          PictureChoice,

          [EnumMember(Value = "multiple_choice")]
          MultipleChoice,

          [EnumMember(Value = "yes_no")]
          YesNo,

          [EnumMember(Value = "number")]
          Number,
        }

        [JsonProperty("id", Required = Required.Always)]
        public string Id;

        [JsonProperty("type", Required = Required.Always)]
        public FieldTypeEnum Type;

        [JsonProperty("ref", Required = Required.Always)]
        public string Ref;
      }

      public class ChoicesType
      {
        [JsonProperty("ids", Required = Required.Always)]
        public List<string> Ids;

        [JsonProperty("labels", Required = Required.Always)]
        public List<string> Labels;
      }

      public class ChoiceType
      {
        [JsonProperty("id", Required = Required.Always)]
        public string Id;

        [JsonProperty("label", Required = Required.Always)]
        public string Label;
      }

      public class ValueClass
      {
        [SuppressMessage("ReSharper", "UnusedMember.Global")]
        [JsonConverter(typeof(StringEnumConverter))]
        public enum ValueTypeEnum
        {
          [EnumMember(Value = "number")]
          Number,

          [EnumMember(Value = "text")]
          Text,

          [EnumMember(Value = "email")]
          Email,

          [EnumMember(Value = "phone_number")]
          PhoneNumber,

          [EnumMember(Value = "choices")]
          Choices,

          [EnumMember(Value = "choice")]
          Choice,

          [EnumMember(Value = "boolean")]
          Boolean,
        }

        [JsonProperty("field", Required = Required.Always)]
        public FieldDescriptorClass Field;

        [JsonProperty("type", Required = Required.Always)]
        public ValueTypeEnum Type;

        [JsonProperty("text", Required = Required.DisallowNull)]
        public string Text;

        [JsonProperty("email", Required = Required.DisallowNull)]
        public string Email;

        [JsonProperty("phone_number", Required = Required.DisallowNull)]
        public string PhoneNumber;

        [JsonProperty("number", Required = Required.DisallowNull)]
        public int Number;

        [JsonProperty("choices", Required = Required.DisallowNull)]
        public ChoicesType Choices;

        [JsonProperty("choice", Required = Required.DisallowNull)]
        public ChoiceType Choice;

        [JsonProperty("boolean", Required = Required.DisallowNull)]
        public bool Boolean;

        public object Value
        {
          get
          {
            switch (Type) {
              case ValueTypeEnum.Number: return Number;
              case ValueTypeEnum.Text: return Text;
              case ValueTypeEnum.Email: return Email;
              case ValueTypeEnum.Boolean: return Boolean;
              case ValueTypeEnum.PhoneNumber: return PhoneNumber;
              case ValueTypeEnum.Choice:
                return new {
                    id = Choice.Id,
                    display = Choice.Label,
                };
              case ValueTypeEnum.Choices:
                return Choices.Ids
                              .Zip(Choices.Labels, (id, label) => (id, label))
                              .Select(x => new { x.id, x.label })
                              .ToArray();
              default: throw new ArgumentOutOfRangeException();
            }
          }
        }
      }

      [JsonProperty("landing_id", Required = Required.Always)]
      public string LandingId;

      [JsonProperty("token", Required = Required.Always)]
      public string Token;

      [JsonProperty("landed_at", Required = Required.Always)]
      [JsonConverter(typeof(DateFormatJsonConverter), "yyyy-MM-ddTHH:mm:ssZ")]
      public DateTime LandedAt;

      [JsonProperty("submitted_at", Required = Required.Always)]
      [JsonConverter(typeof(DateFormatJsonConverter), "yyyy-MM-ddTHH:mm:ssZ")]
      public DateTime SubmittedAt;

      [JsonProperty("metadata", Required = Required.Always)]
      public MetadataClass Metadata;

      [JsonProperty("hidden", Required = Required.Always)]
      public IDictionary<string, string> Hidden;

      [JsonProperty("answers", Required = Required.AllowNull)]
      public IList<ValueClass> Answers;
    }


    private readonly string _token;

    public TypeFormAPI(string token)
    {
      _token = token;
    }

    public async Task<List<TypeFormResponse>> GetRecentResponses(string surveyId)
    {
      string url = UrlUtils.CreateUrl($"https://api.typeform.com/forms/{surveyId}/responses", new {
          key = _token,
          completed = "true",
          sort = "submitted_at,desc",
          page_size = 1000,
      });

      var resp = await Requests.GetAsync(url,
                                         authBearerToken: _token);

      if (!resp.Success)
        throw new Exception("Unable to load responses: " + resp.StatusCode);

      var data = resp.Json;

      var responses = data["items"].ToObject<IList<TypeFormResponse>>()
                                   .Where(x => x.Answers != null);

      return responses.ToList();
    }
  }
}