using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using KDLib;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Utilities.Encoders;
using Object = Google.Apis.Storage.v1.Data.Object;

namespace KDAPILib.Google.CloudStorage
{
  public class GoogleCloudStorage
  {
    public class POSTUploadSession
    {
      public string Url { get; set; }
      public Dictionary<string, string> Parameters { get; set; }
    }

    private readonly string _bucketSuffix;
    private readonly ServiceAccountCredential _underlyingCredential;
    private readonly UrlSigner _urlSigner;
    private readonly GoogleCredential _googleCredential;
    private readonly StorageClient _client;

    public GoogleCloudStorage(string keyFile, string bucketSuffix = "")
    {
      _bucketSuffix = bucketSuffix;
      _googleCredential = GoogleCredential.FromFile(keyFile);
      _underlyingCredential = (ServiceAccountCredential)_googleCredential.UnderlyingCredential;
      _urlSigner = UrlSigner.FromServiceAccountCredential(_underlyingCredential);
      _client = StorageClient.Create(_googleCredential);
    }

    public GoogleCredential Credentials => _googleCredential;

    public string CreateSignedGETUrl(string gsurl, TimeSpan ttl, string downloadFileName)
    {
      UrlUtils.ParseUri(gsurl, out var scheme, out var _, out var _, out var bucket, out var _, out var path);
      Debug.Assert(scheme == "gs");
      path = path.Substring(1);

      return CreateSignedGETUrl(bucket, path, ttl, downloadFileName);
    }

    public string CreateSignedGETUrl(string bucket, string key, TimeSpan ttl, string downloadFileName)
    {
      var contentDisposition = $"attachment; filename=\"{Uri.EscapeDataString(downloadFileName)}\"";

      var url = _urlSigner.Sign(bucket + _bucketSuffix, key, ttl, HttpMethod.Get);

      url = UrlUtils.ExtendUrl(url, new Dictionary<string, string> {
          { "response-content-disposition", contentDisposition }
      });

      return url;
    }

    public POSTUploadSession CreateSignedPOSTData(string bucket, string key, long minSize, long maxSize, TimeSpan ttl)
    {
      // string acl = "bucket-owner-read";
      // string acl = "bucket-owner-full-control";
      // string acl = "project-private";

      var conditions = new JArray {
          // new JObject { { "acl", acl } },
          new JObject { { "bucket", bucket + _bucketSuffix } },

          new JArray { "eq", "$key", key },
          // new JArray { "eq", "$Content-Type", "text/plain" },
          // new JArray { "starts-with", "$key", "" },
          // new JArray { "starts-with", "$Content-Type", "" },
          new JArray { "content-length-range", minSize, maxSize },

          // new JArray { "starts-with", "$x-goog-meta-filename", "" },
          // new JArray { "starts-with", "$x-goog-meta-lastmod", "" },
      };

      var policy = new JObject {
          { "expiration", DateTime.UtcNow + ttl },
          { "conditions", conditions }
      };

      var policyStr = policy.ToString(Formatting.None);

      var policyBase64Bytes = Base64.Encode(Encoding.UTF8.GetBytes(policyStr));
      var policyBase64String = Encoding.ASCII.GetString(policyBase64Bytes);

      var signatureBase64String = _underlyingCredential.CreateSignature(policyBase64Bytes);

      return new POSTUploadSession {
          Url = $"https://{bucket + _bucketSuffix}.storage.googleapis.com",
          Parameters = new Dictionary<string, string> {
              { "bucket", bucket + _bucketSuffix },
              { "key", key },
              // { "acl", acl },
              // { "Content-Type", "text/plain" },
              { "GoogleAccessId", _underlyingCredential.Id },
              { "policy", policyBase64String },
              { "signature", signatureBase64String },
          }
      };
    }

    public Task<Object> GetObject(string gsurl)
    {
      UrlUtils.ParseUri(gsurl, out var scheme, out var _, out var _, out var bucket, out var _, out var path);
      Debug.Assert(scheme == "gs");
      path = path.Substring(1);

      return GetObject(bucket, path);
    }

    public async Task<Object> GetObject(string bucket, string name)
    {
      return await _client.GetObjectAsync(GetBucketName(bucket), name);
    }

    public async Task<Object> PutObject(string bucket, string name, string contentType, byte[] data, string downloadName = null, bool asAttachment = false)
    {
      var obj = new Object {
          Bucket = GetBucketName(bucket),
          Name = name,
          ContentType = contentType,
      };

      if (downloadName != null) {
        if (asAttachment)
          obj.ContentDisposition = $"attachment; filename={HttpUtility.UrlEncode(downloadName)};";
        else
          obj.ContentDisposition = $"filename={HttpUtility.UrlEncode(downloadName)};";
      }

      using var ms = new MemoryStream(data);
      return await _client.UploadObjectAsync(obj, ms);
    }

    public async Task CopyObject(string srcBucket, string srcPath, string dstBucket, string dstPath)
    {
      await _client.CopyObjectAsync(GetBucketName(srcBucket),
                                    srcPath,
                                    GetBucketName(dstBucket),
                                    dstPath);
    }

    public async Task DeleteObject(string bucket, string objectName)
    {
      await _client.DeleteObjectAsync(GetBucketName(bucket), objectName);
    }

    public string GetPublicUrl(Object obj) => $"https://storage.googleapis.com/{obj.Bucket}/{obj.Name}";

    private string GetBucketName(string name) => name + _bucketSuffix;
  }
}