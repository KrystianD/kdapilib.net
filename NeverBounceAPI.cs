﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KDLib;
using RequestsNET;

namespace KDAPILib.NeverBounce
{
  public class NeverBounceAPI
  {
    private readonly string _token;

    public class NeverBounceAPIException : Exception
    {
    }

    public class ValidationResult
    {
      public enum ResultEnum
      {
        Valid,
        Invalid,
        Disposable,
        Catchall,
        Unknown,
      }

      public readonly ResultEnum Result;
      public readonly string Suggestion;

      public bool HasSuggestion => Suggestion != null;

      public ValidationResult(ResultEnum result, string suggestion)
      {
        Suggestion = suggestion;
        Result = result;
      }
    }

    public NeverBounceAPI(string token)
    {
      _token = token;
    }

    public async Task<ValidationResult> ValidateEmail(string email)
    {
      var res = await Requests.GetAsync("https://api.neverbounce.com/v4/single/check", new Dictionary<string, string>() {
          ["key"] = _token,
          ["email"] = email,
          ["timeout"] = "10",
      });

      var json = res.Json;

      string status = json.Value<string>("status");
      string result = json.Value<string>("result");
      string suggestedCorrection = json.Value<string>("suggested_correction");

      if (status != "success")
        throw new NeverBounceAPIException();

      ValidationResult.ResultEnum r;

      switch (result) {
        case "valid":
          r = ValidationResult.ResultEnum.Valid;
          break;
        case "invalid":
          r = ValidationResult.ResultEnum.Invalid;
          break;
        case "disposable":
          r = ValidationResult.ResultEnum.Disposable;
          break;
        case "catchall":
          r = ValidationResult.ResultEnum.Catchall;
          break;
        case "unknown":
          r = ValidationResult.ResultEnum.Unknown;
          break;
        default: throw new NeverBounceAPIException();
      }

      if (string.IsNullOrWhiteSpace(suggestedCorrection))
        suggestedCorrection = null;

      return new ValidationResult(r, suggestedCorrection);
    }
  }
}