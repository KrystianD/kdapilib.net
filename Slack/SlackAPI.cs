﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KDLib;
using Newtonsoft.Json.Linq;
using RequestsNET;

namespace KDAPILib.Slack
{
  public class SlackAPI
  {
    private readonly string _token;
    public readonly string SenderName;
    public readonly string SenderAvatar;

    public SlackAPI(string token, string senderName = null, string senderAvatar = null)
    {
      _token = token;
      SenderName = senderName;
      SenderAvatar = senderAvatar;
    }

    public Task<string> PostMessage(string channel, SlackAPIMessage apiMessage)
    {
      var data = apiMessage.CreateAPIData(this);
      data["as_user"] = false;
      data["channel"] = channel;
      return SendCommand("chat.postMessage", data);
    }

    public Task SendToUrl(string url, SlackAPIMessage apiMessage)
    {
      var data = apiMessage.CreateAPIData(this);
      data["as_user"] = false;
      return Requests.PostAsync(url, json: JToken.FromObject(data));
    }

    public async Task<List<SlackUser>> GetUsers()
    {
      var t = await PerformGET("users.list");

      var members = t["members"].ToObject<List<SlackUser>>();
      return members;
    }

    private async Task<string> SendCommand(string method, object data)
    {
      var res = await SendRequest(method, data);
      var ts = res.SelectToken("ts");
      return ts?.Value<string>();
    }

    private async Task<JObject> PerformGET(string method)
    {
      var res = await Requests.GetAsync($"https://slack.com/api/{method}",
                                        headers: new Dictionary<string, string>() {
                                            { "Authorization", $"Bearer {_token}" }
                                        });

      return (JObject) res.Json;
    }

    private async Task<JObject> SendRequest(string method, object data = null)
    {
      var res = await Requests.PostAsync($"https://slack.com/api/{method}",
                                         json: JToken.FromObject(data),
                                         headers: new Dictionary<string, string>() {
                                             { "Authorization", $"Bearer {_token}" }
                                         });

      return (JObject) res.Json;
    }
  }

  public class SlackAPIMessage
  {
    public enum MessageTypeEnum
    {
      Default,
      Ephemeral,
    }

    public string Text { get; set; }
    public string UserName { get; set; }
    public string UserAvatar { get; set; }
    public List<SlackAPIMessageAttachment> Attachments { get; set; } = new List<SlackAPIMessageAttachment>();

    public MessageTypeEnum MessageType { get; set; } = MessageTypeEnum.Default;
    public bool ReplaceOriginal { get; set; } = false;

    public SlackAPIMessage(string text = "")
    {
      Text = text;
    }

    public SlackAPIMessageAttachment AddAttachment(string text = "", string callbackId = "", string title = null,
                                                   string color = null)
    {
      var a = new SlackAPIMessageAttachment();
      a.Text = text;
      a.Title = title;
      a.CallbackId = callbackId;
      a.Color = color;
      Attachments.Add(a);
      return a;
    }

    public JObject CreateAPIData(SlackAPI api)
    {
      var userName = string.IsNullOrEmpty(UserName) ? api.SenderName : UserName;
      var userAvatar = string.IsNullOrEmpty(UserAvatar) ? api.SenderAvatar : UserAvatar;

      var data = new JObject();
      data["text"] = Text;
      data["attachments"] = JArray.FromObject(Attachments.Select(x => x.ToJson()));
      data["username"] = userName;
      data["icon_emoji"] = !string.IsNullOrEmpty(userAvatar) && userAvatar[0] == ':' ? userAvatar : null;
      data["icon_url"] = !string.IsNullOrEmpty(userAvatar) && userAvatar[0] != ':' ? userAvatar : null;

      if (MessageType == MessageTypeEnum.Ephemeral)
        data["response_type"] = "ephemeral";

      if (!ReplaceOriginal)
        data["replace_original"] = false;

      return data;
    }
  }

  public class SlackAPIMessageAttachment
  {
    public enum SlackAPIButtonStyle
    {
      Default,
      Primary,
      Danger
    }

    public string Text { get; set; }
    public string Title { get; set; }
    public string CallbackId { get; set; }
    public string Footer { get; set; }
    public string FooterIcon { get; set; }
    public string Color { get; set; }

    private JArray _fields = new JArray();
    private JArray _actions = new JArray();

    public void AddField(string label, string content, bool _short = true)
    {
      _fields.Add(JObject.FromObject(new Dictionary<string, object>() {
          { "title", label },
          { "value", content },
          { "short", _short },
      }));
    }

    public void AddAction(string name, string value, string label,
                          string url = null,
                          SlackAPIButtonStyle style = SlackAPIButtonStyle.Default)
    {
      string styleStr;
      switch (style) {
        case SlackAPIButtonStyle.Default:
          styleStr = "default";
          break;
        case SlackAPIButtonStyle.Primary:
          styleStr = "primary";
          break;
        case SlackAPIButtonStyle.Danger:
          styleStr = "danger";
          break;
        default: throw new ArgumentOutOfRangeException(nameof(style), style, null);
      }

      var d = new Dictionary<string, object>() {
          { "name", name },
          { "text", label },
          { "type", "button" },
          { "style", styleStr },
      };

      if (value != null)
        d["value"] = value;
      if (url != null)
        d["url"] = url;

      _actions.Add(JObject.FromObject(d));
    }

    public void AddMenu(string name, string label, List<KeyValuePair<string, string>> options)
    {
      _actions.Add(JObject.FromObject(new Dictionary<string, object>() {
          { "name", name },
          { "text", label },
          { "type", "select" },
          { "options", options.Select(x => new { text = x.Key, value = x.Value }) },
      }));
    }

    public JObject ToJson()
    {
      return JObject.FromObject(new Dictionary<string, object>() {
              { "text", Text },
              { "title", Title },
              { "fallback", "" },
              { "callback_id", CallbackId },
              { "attachment_type", "default" },
              { "actions", _actions },
              { "fields", _fields },
              { "footer", Footer },
              { "footer_icon", FooterIcon },
              { "color", Color },
          }
      );
    }

    public static SlackAPIMessageAttachment FromJson(JObject obj)
    {
      return new SlackAPIMessageAttachment() {
          Text = obj["text"].Value<string>(),
          Title = obj["title"].Value<string>(),
          CallbackId = obj["callback_id"].Value<string>(),
          _actions = obj["actions"].Value<JArray>(),
          _fields = obj["fields"].Value<JArray>(),
          Footer = obj["footer"].Value<string>(),
          FooterIcon = obj["footer_icon"].Value<string>(),
          Color = obj["color"].Value<string>(),
      };
    }
  }
}