﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using KDLib;
using Newtonsoft.Json.Linq;
using RequestsNET;

namespace KDAPILib.Mailjet
{
  public class MailjetAPI
  {
    private readonly string publicKey;
    private readonly string secretKey;

    public string OverrideRecipient { get; set; }

    private JObject MailAddressToJObject(MailAddress from)
    {
      if (string.IsNullOrWhiteSpace(from.DisplayName))
        return new JObject { { "Email", from.Address } };
      else
        return new JObject { { "Email", from.Address }, { "Name", from.DisplayName } };
    }

    public MailjetAPI(string publicKey, string secretKey)
    {
      this.publicKey = publicKey;
      this.secretKey = secretKey;
    }

    public async Task<string> SendTemplate(int templateId,
                                           MailAddress from,
                                           List<MailAddress> to,
                                           List<MailAddress> cc = null,
                                           List<MailAddress> bcc = null,
                                           MailAddress replyTo = null,
                                           Dictionary<string, string> parameters = null)
    {
      var message = new JObject {
          { "From", MailAddressToJObject(from) },
          { "To", JArray.FromObject(to.Select(MailAddressToJObject)) },
          { "TemplateID", templateId },
          { "TemplateLanguage", true },
          { "CustomID", Guid.NewGuid().ToString() }
      };

      if (cc != null)
        message["Cc"] = JArray.FromObject(cc.Select(MailAddressToJObject));

      if (bcc != null)
        message["Bcc"] = JArray.FromObject(bcc.Select(MailAddressToJObject));

      if (replyTo != null)
        message["ReplyTo"] = MailAddressToJObject(replyTo);

      if (parameters != null)
        message["Variables"] = JObject.FromObject(parameters);

      var json = new JObject {
          { "Messages", new JArray { message } },
      };

      var resp = await Requests.PostAsync(
          "https://api.mailjet.com/v3.1/send",
          json: json,
          authUser: publicKey,
          authPass: secretKey);

      if (!(resp.Json is JObject data))
        throw new Exception("Invalid response");

      var messageId = data["Messages"][0]["To"][0]["MessageID"].Value<string>();

      return messageId;
    }
  }
}