﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Minio;
using Minio.DataModel;

namespace KDAPILib.Minio
{
  public class MinioAPI
  {
    private static MinioClient minio;
    private string _suffix;

    public MinioAPI(string host, string accessKey, string secretKey, string suffix)
    {
      minio = new MinioClient()
              .WithHttpClient(new HttpClient())
              .WithEndpoint(host)
              .WithCredentials(accessKey, secretKey)
              .WithSSL();
      _suffix = suffix;
    }

    public Task UploadFile(string bucketName, string objectName, byte[] payload, string contentType = null)
    {
      return minio.PutObjectAsync(bucketName + _suffix, objectName, new MemoryStream(payload), payload.Length, contentType ?? "application/octet-stream");
    }

    public async Task<Tuple<string, Dictionary<string, string>>> GetUploadLink(string bucketName, string objectName)
    {
      var pp = new PostPolicy();
      pp.SetBucket(bucketName + _suffix);
      pp.SetKey(objectName);
      pp.SetExpires(DateTime.UtcNow + TimeSpan.FromHours(20));
      pp.SetContentRange(3, 10);
      var res = await minio.PresignedPostPolicyAsync(pp);
      return Tuple.Create(res.Item1.ToString(), res.Item2);
    }

    public Task DeleteFile(string bucketName, string objectName)
    {
      return minio.RemoveObjectAsync(bucketName + _suffix, objectName);
    }

    public Task<string> CreateDownloadLink(string bucketName, string objectName, string downloadFileName, TimeSpan? ttl = null, bool downloadAttachment = false)
    {
      if (ttl == null) ttl = TimeSpan.FromDays(7);

      Dictionary<string, string> _params = new Dictionary<string, string>();
      if (downloadAttachment)
        _params.Add("response-content-disposition", $"attachment; filename=\"{Uri.EscapeDataString(downloadFileName)}\"");
      return minio.PresignedGetObjectAsync(bucketName + _suffix, objectName, (int)ttl.Value.TotalSeconds, _params);
    }
  }
}