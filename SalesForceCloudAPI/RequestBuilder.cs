using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using KDAPILib.Builders;
using KDAPILib.SalesForceCloudAPI.API;
using KDAPILib.SalesForceCloudAPI.Objects;
using KDLib;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KDAPILib.SalesForceCloudAPI
{
  public interface IRequest
  {
    string Method { get; set; }
    Dictionary<string, string> Headers { get; set; }
    string Url { get; set; }
    JToken Body { get; set; }

    Action<API.CompositeSubsequentResult> ResponseProcessor { get; set; }
  }

  public class Request<TResult> : IRequest
  {
    public string Method { get; set; }
    public Dictionary<string, string> Headers { get; set; }
    public string Url { get; set; }
    public Action<CompositeSubsequentResult> ResponseProcessor { get; set; }
    public JToken Body { get; set; }

    public readonly TaskCompletionSource<TResult> ResponseTask = new TaskCompletionSource<TResult>();
  }

  public class SalesForceError
  {
    [JsonProperty("message", Required = Required.Always)]
    public string Message { get; set; }

    [JsonProperty("errorCode", Required = Required.Always)]
    public string ErrorCode { get; set; }

    [JsonProperty("fields", Required = Required.Default)]
    public List<string> Fields { get; set; }
  }

  public class SalesForceRequestException : Exception
  {
    public readonly SalesForceError Error;

    public SalesForceRequestException(SalesForceError error)
        : base($"SalesForce request error: {JsonConvert.SerializeObject(error)}")
    {
      Error = error;
    }
  }

  public class GetObjectResponse<T>
  {
    public T Object;
    public bool NotModified;

    public static GetObjectResponse<T> ObjectNotModified => new GetObjectResponse<T>() { NotModified = true };
    public static GetObjectResponse<T> FromObject(T obj) => new GetObjectResponse<T>() { NotModified = false, Object = obj };
  }

  public class FieldsCollection<T>
  {
    private readonly List<Expression<Func<T, object>>> _fields = new List<Expression<Func<T, object>>>();
    private readonly HashSet<string> _jsonFields = new HashSet<string>();

    public static FieldsCollection<T> AllFields = new FieldsCollection<T>();

    public void AddField(Expression<Func<T, object>> field)
    {
      _fields.Add(field);
      _jsonFields.Add(ObjectUtils.GetFieldJsonName(field));
    }

    public bool HasField(string jsonFieldName) => _jsonFields.Count == 0 || _jsonFields.Contains(jsonFieldName);

    public bool HasField(Expression<Func<T, object>> field)
    {
      var name = ObjectUtils.GetFieldJsonName(field);
      return HasField(name);
    }

    public List<string> GetJSONNames() => _jsonFields.ToList();

    public JObject ConvertObjectToLimitedJSON(T obj)
    {
      var fullBody = JObject.FromObject(obj);
      var partialBody = new JObject(fullBody.Properties().Where(x => HasField(x.Name)));
      return partialBody;
    }

    public static FieldsCollection<T> WithFields(params Expression<Func<T, object>>[] fields)
    {
      var fc = new FieldsCollection<T>();
      foreach (var field in fields)
        fc.AddField(field);
      return fc;
    }
  }

  public static class RequestBuilder
  {
    public static Request<GetObjectResponse<T>> GetObject<T>(string objectType,
                                                             string id,
                                                             DateTime? ifModifiedSince = null) where T : BaseObject
    {
      var headers = new Dictionary<string, string>();

      if (ifModifiedSince.HasValue)
        headers["If-Modified-Since"] = ifModifiedSince.Value.ToString("R");

      var request = new Request<GetObjectResponse<T>>() {
          Url = $"/services/data/v39.0/sobjects/{objectType}/{id}?" +
                UrlUtils.UrlEncodeKeyValuePairs(new Dictionary<string, string>() {
                    ["fields"] = ObjectUtils.GetAllFields<T>().JoinString(","),
                }),
          Headers = headers,
          Method = "GET",
      };

      request.ResponseProcessor = compositeSubsequentResult => {
        switch (compositeSubsequentResult.StatusCode) {
          case HttpStatusCode.OK:
            request.ResponseTask.SetResult(GetObjectResponse<T>.FromObject(compositeSubsequentResult.Body.ToObject<T>()));
            break;
          case HttpStatusCode.NotModified:
            request.ResponseTask.SetResult(GetObjectResponse<T>.ObjectNotModified);
            break;
          default:
            request.ResponseTask.SetException(Utils.ParseError(compositeSubsequentResult.Body));
            break;
        }
      };

      return request;
    }

    public static Request<List<T>> GetObjects<T>(string objectType,
                                                               IList<string> ids) where T : BaseObject
    {
      var headers = new Dictionary<string, string>();

      var request = new Request<List<T>>() {
          Url = $"/services/data/v42.0/composite/sobjects/{objectType}"  ,
          Body = JToken.FromObject(new {
              ids = ids,
              fields = ObjectUtils.GetAllFields<T>(),
          }),
          Headers = headers,
          Method = "POST",
      };

      request.ResponseProcessor = compositeSubsequentResult => {
        switch (compositeSubsequentResult.StatusCode) {
          case HttpStatusCode.OK:
            request.ResponseTask.SetResult(compositeSubsequentResult.Body.ToObject<List<T>>());
            break;
          default:
            request.ResponseTask.SetException(Utils.ParseError(compositeSubsequentResult.Body));
            break;
        }
      };

      return request;
    }

    public static Request<GetObjectResponse<T>> GetObjectByExternalId<T>(string objectType, Expression<Func<T, object>> externalIdFunc, string externalId, FieldsCollection<T> fields = null) where T : BaseObject
      => GetObjectByExternalId<T>(objectType, ObjectUtils.GetFieldJsonName<T>(externalIdFunc), externalId, fields);

    public static Request<GetObjectResponse<T>> GetObjectByExternalId<T>(string objectType, string externalFieldName, string externalId, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var request = new Request<GetObjectResponse<T>>() {
          Url = $"/services/data/v39.0/sobjects/{objectType}/{externalFieldName}/{externalId}?" +
                UrlUtils.UrlEncodeKeyValuePairs(new Dictionary<string, string>() {
                    ["fields"] = (fields?.GetJSONNames() ?? ObjectUtils.GetAllFields<T>()).JoinString(","),
                }),
          Method = "GET",
      };

      request.ResponseProcessor = compositeSubsequentResult => {
        switch (compositeSubsequentResult.StatusCode) {
          case HttpStatusCode.OK:
            request.ResponseTask.SetResult(GetObjectResponse<T>.FromObject(compositeSubsequentResult.Body.ToObject<T>()));
            break;
          default:
            request.ResponseTask.SetException(Utils.ParseError(compositeSubsequentResult.Body));
            break;
        }
      };

      return request;
    }

    public static Request<List<T>> QueryAll<T>(string query) where T : BaseObject
    {
      var request = new Request<List<T>>() {
          Url = "/services/data/v39.0/queryAll?" +
                UrlUtils.UrlEncodeKeyValuePairs(new Dictionary<string, string>() {
                    ["q"] = query,
                }),
          Method = "GET",
      };

      request.ResponseProcessor = compositeSubsequentResult => {
        if (compositeSubsequentResult.StatusCode == HttpStatusCode.OK) {
          var res = compositeSubsequentResult.Body;

          var nextRecordsUrl = (string)res["nextRecordsUrl"];
          var totalSize = res["totalSize"].Value<int>();
          var done = res["done"].Value<bool>();
          var records = res["records"].ToObject<List<T>>();

          Trace.Assert(done == true, "More data available");
          Trace.Assert(totalSize == records.Count);

          request.ResponseTask.SetResult(records);
        }
        else {
          request.ResponseTask.SetException(Utils.ParseError(compositeSubsequentResult.Body));
        }
      };

      return request;
    }

    public static Request<GetChangesResponse> GetChanges(string objectType, DateTime startDate, DateTime endDate)
    {
      var request = new Request<GetChangesResponse>() {
          Url = $"/services/data/v39.0/sobjects/{objectType}/updated/?" +
                UrlUtils.UrlEncodeKeyValuePairs(new Dictionary<string, string>() {
                    ["start"] = startDate.ToString("yyyy-MM-ddTHH:mm:ss+00:00"),
                    ["end"] = endDate.ToString("yyyy-MM-ddTHH:mm:ss+00:00"),
                }),
          Method = "GET",
      };

      request.ResponseProcessor = compositeSubsequentResult => {
        if (compositeSubsequentResult.StatusCode == HttpStatusCode.OK)
          request.ResponseTask.SetResult(compositeSubsequentResult.Body.ToObject<GetChangesResponse>());
        else
          request.ResponseTask.SetException(Utils.ParseError(compositeSubsequentResult.Body));
      };

      return request;
    }

    public static Request<GetDeletedResponse> GetDeleted(string objectType, DateTime startDate, DateTime endDate)
    {
      var request = new Request<GetDeletedResponse>() {
          Url = $"/services/data/v39.0/sobjects/{objectType}/deleted/?" +
                UrlUtils.UrlEncodeKeyValuePairs(new Dictionary<string, string>() {
                    ["start"] = startDate.ToString("yyyy-MM-ddTHH:mm:ss+00:00"),
                    ["end"] = endDate.ToString("yyyy-MM-ddTHH:mm:ss+00:00"),
                }),
          Method = "GET",
      };

      request.ResponseProcessor = compositeSubsequentResult => {
        if (compositeSubsequentResult.StatusCode == HttpStatusCode.OK)
          request.ResponseTask.SetResult(compositeSubsequentResult.Body.ToObject<GetDeletedResponse>());
        else
          request.ResponseTask.SetException(Utils.ParseError(compositeSubsequentResult.Body));
      };

      return request;
    }

    public static Request<bool> Update<T>(string objectType, string id, T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      if (fields == null)
        fields = FieldsCollection<T>.AllFields;

      var partialBody = fields.ConvertObjectToLimitedJSON(obj);

      var request = new Request<bool>() {
          Url = $"/services/data/v39.0/sobjects/{objectType}/{id}",
          Method = "PATCH",
          Body = partialBody,
      };

      request.ResponseProcessor = compositeSubsequentResult => {
        if (compositeSubsequentResult.StatusCode == HttpStatusCode.NoContent)
          request.ResponseTask.SetResult(true);
        else
          request.ResponseTask.SetException(Utils.ParseError(compositeSubsequentResult.Body));
      };

      return request;
    }

    public static Request<bool> UpdateByExternalId<T>(string objectType, Expression<Func<T, object>> externalIdFunc, string externalId, T obj, FieldsCollection<T> fields = null) where T : BaseObject
      => UpdateByExternalId(objectType, ObjectUtils.GetFieldJsonName<T>(externalIdFunc), externalId, obj, fields);

    public static Request<bool> UpdateByExternalId<T>(string objectType, string externalFieldName, string externalId, T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      if (fields == null)
        fields = FieldsCollection<T>.AllFields;

      var partialBody = fields.ConvertObjectToLimitedJSON(obj);

      var request = new Request<bool>() {
          Url = $"/services/data/v39.0/sobjects/{objectType}/{externalFieldName}/{externalId}",
          Method = "PATCH",
          Body = partialBody,
      };

      request.ResponseProcessor = compositeSubsequentResult => {
        if (compositeSubsequentResult.StatusCode == HttpStatusCode.NoContent)
          request.ResponseTask.SetResult(true);
        else
          request.ResponseTask.SetException(Utils.ParseError(compositeSubsequentResult.Body));
      };

      return request;
    }


    public static Request<string> Create<T>(string objectType, T obj, FieldsCollection<T> fields) where T : BaseObject
    {
      if (fields == null)
        fields = FieldsCollection<T>.AllFields;

      var partialBody = fields.ConvertObjectToLimitedJSON(obj);

      var request = new Request<string>() {
          Url = $"/services/data/v39.0/sobjects/{objectType}/",
          Method = "POST",
          Body = partialBody,
      };

      request.ResponseProcessor = compositeSubsequentResult => {
        if (compositeSubsequentResult.StatusCode == HttpStatusCode.Created)
          request.ResponseTask.SetResult(compositeSubsequentResult.Body["id"].Value<string>());
        else
          request.ResponseTask.SetException(Utils.ParseError(compositeSubsequentResult.Body));
      };

      return request;
    }

    public static Request<string> PostChatterItem(string objectId, IFeedItem item)
    {
      var body = item.Build();
      body["ParentId"] = objectId;

      var request = new Request<string>() {
          Url = "/services/data/v36.0/sobjects/FeedItem",
          Method = "POST",
          Body = body,
      };

      request.ResponseProcessor = compositeSubsequentResult => {
        if (compositeSubsequentResult.StatusCode == HttpStatusCode.Created)
          request.ResponseTask.SetResult(compositeSubsequentResult.Body["id"].Value<string>());
        else
          request.ResponseTask.SetException(Utils.ParseError(compositeSubsequentResult.Body));
      };

      return request;
    }
  }
}