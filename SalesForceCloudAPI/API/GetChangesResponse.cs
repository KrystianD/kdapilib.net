using System;
using System.Collections.Generic;
using KDAPILib.SalesForceCloudAPI.Objects;
using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.API
{
  public class GetChangesResponse
  {
    [JsonProperty("ids")]
    public List<string> Ids;

    [JsonProperty("latestDateCovered")]
    [JsonConverter(typeof(SFDateTimeConverter))]
    public DateTime LatestDateCovered;
  }

  public class GetDeletedResponse
  {
    public class DeletedRecord
    {
      [JsonProperty("deletedDate")]
      [JsonConverter(typeof(SFDateTimeConverter))]
      public DateTime DeletedDate;

      [JsonProperty("id")]
      public string Id;
    }

    [JsonProperty("deletedRecords")]
    public List<DeletedRecord> DeletedRecords;

    [JsonProperty("latestDateCovered")]
    [JsonConverter(typeof(SFDateTimeConverter))]
    public DateTime LatestDateCovered;
  }
}