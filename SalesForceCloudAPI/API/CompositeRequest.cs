using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KDAPILib.SalesForceCloudAPI.API
{
  public class CompositeRequest
  {
    [JsonProperty("compositeRequest")]
    public List<CompositeSubsequentRequest> compositeRequests = new List<CompositeSubsequentRequest>();
  }

  public class CompositeResponse
  {
    [JsonProperty("compositeResponse")]
    public List<CompositeSubsequentResult> compositeResponses;
  }

  public class CompositeSubsequentRequest
  {
    [JsonProperty("body")]
    public JToken Body;

    [JsonProperty("httpHeaders")]
    public Dictionary<string, string> Headers;

    [JsonProperty("method")]
    public string Method;

    [JsonProperty("referenceId")]
    public string ReferenceId;

    [JsonProperty("url")]
    public string Url;
  }

  public class CompositeSubsequentResult
  {
    [JsonProperty("body")]
    public JToken Body;

    [JsonProperty("httpHeaders")]
    public Dictionary<string, string> Headers;

    [JsonProperty("httpStatusCode")]
    private int _statusCode;

    [JsonProperty("referenceID")]
    public string ReferenceId;

    [JsonIgnore]
    public HttpStatusCode StatusCode => (HttpStatusCode)_statusCode;
  }
}