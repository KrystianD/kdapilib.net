namespace KDAPILib.SalesForceCloudAPI.API
{
  public static class Constants
  {
    public const int MaxCompositeSubrequests = 25;
  }
}