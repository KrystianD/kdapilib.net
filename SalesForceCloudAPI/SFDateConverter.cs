using System;
using KDLib.JsonConverters;
using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.Objects
{
  public class SFDateConverter : DateFormatJsonConverter
  {
    public SFDateConverter() : base("yyyy-MM-dd") { }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
      DateTime? date = (DateTime?)base.ReadJson(reader, typeof(DateTime?), existingValue, serializer);
      if (date == null)
        return null;
      return DateTime.SpecifyKind(date.Value, DateTimeKind.Utc);
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
      base.WriteJson(writer, value, serializer);
    }
  }
}