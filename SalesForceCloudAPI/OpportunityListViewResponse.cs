using System.Collections.Generic;
using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI
{
  public class OpportunityListViewResponse
  {
    public class Column
    {
      public string fieldNameOrPath;
      public string label;
    }

    public class Record
    {
      public class ColumnValue
      {
        [JsonProperty("fieldNameOrPath")]
        public string fieldNameOrPath;

        [JsonProperty("value")]
        public string value;

        public override string ToString()
        {
          return $"{fieldNameOrPath} = {value}";
        }
      }

      [JsonProperty("columns")]
      public List<ColumnValue> Columns;
    }

    [JsonProperty("columns")]
    public List<Column> Columns;

    [JsonProperty("records")]
    public List<Record> Records;
  }
}