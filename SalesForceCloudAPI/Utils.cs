using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KDAPILib.SalesForceCloudAPI
{
  internal static class Utils
  {
    public static Exception ParseError(JToken body)
    {
      if (body is JArray array) {
        return new SalesForceRequestException(array[0].ToObject<SalesForceError>());
      }
      else {
        return new Exception($"unknown error: {JsonConvert.SerializeObject(body)}");
      }
    }
  }
}