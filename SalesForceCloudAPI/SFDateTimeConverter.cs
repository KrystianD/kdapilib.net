using System;
using KDLib.JsonConverters;
using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.Objects
{
  public class SFDateTimeConverter : DateFormatJsonConverter
  {
    public SFDateTimeConverter() : base("yyyy-MM-ddTHH:mm:ss.fffzzz") { }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
      DateTimeOffset? date = (DateTimeOffset?)base.ReadJson(reader, typeof(DateTimeOffset?), existingValue, serializer);
      return date?.UtcDateTime;
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
      base.WriteJson(writer, value, serializer);
    }
  }
}