using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using KDAPILib.Builders;
using KDAPILib.SalesForceCloudAPI.API;
using KDAPILib.SalesForceCloudAPI.Objects;
using Newtonsoft.Json.Linq;

namespace KDAPILib.SalesForceCloudAPI
{
  public class CompositeRequest
  {
    private readonly SalesForceCloudAPI _api;

    private readonly API.CompositeRequest _data = new API.CompositeRequest();
    private readonly List<IRequest> _requests = new List<IRequest>();

    public CompositeRequest(SalesForceCloudAPI api)
    {
      _api = api;
    }

    private void AddRequest(IRequest req)
    {
      _data.compositeRequests.Add(new CompositeSubsequentRequest() {
          Url = req.Url,
          Method = req.Method,
          Headers = req.Headers,
          Body = req.Body,
          ReferenceId = $"{_requests.Count}",
      });
      _requests.Add(req);
    }

    private void ProcessResponse(JToken resJson)
    {
      var resp = resJson.ToObject<CompositeResponse>();

      foreach (var result in resp.compositeResponses) {
        var idx = int.Parse(result.ReferenceId);
        _requests[idx].ResponseProcessor(result);
      }
    }

    public async Task Execute()
    {
      var res = await _api.DoPost("services/data/v42.0/composite", JToken.FromObject(_data));
      ProcessResponse(res.Json);
    }

    public Task<GetObjectResponse<T>> GetObject<T>(string objectType, string id, DateTime? ifModifiedSince = null) where T : BaseObject
    {
      var req = RequestBuilder.GetObject<T>(objectType, id, ifModifiedSince);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public Task<List<T>> GetObjects<T>(string objectType, IList<string> ids) where T : BaseObject
    {
      var req = RequestBuilder.GetObjects<T>(objectType, ids);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public Task<GetObjectResponse<T>> GetObjectByExternalId<T>(string objectType, Expression<Func<T, object>> externalIdFunc, string externalId, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var req = RequestBuilder.GetObjectByExternalId<T>(objectType, externalIdFunc, externalId, fields);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public Task<GetObjectResponse<T>> GetObjectByExternalId<T>(string objectType, string externalFieldName, string externalId, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var req = RequestBuilder.GetObjectByExternalId<T>(objectType, externalFieldName, externalId, fields);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public Task<List<T>> QueryAll<T>(string query) where T : BaseObject
    {
      var req = RequestBuilder.QueryAll<T>(query);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public Task<GetChangesResponse> GetChanges(string objectType, DateTime startDate, DateTime endDate)
    {
      var req = RequestBuilder.GetChanges(objectType, startDate, endDate);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public Task<GetDeletedResponse> GetDeleted(string objectType, DateTime startDate, DateTime endDate)
    {
      var req = RequestBuilder.GetDeleted(objectType, startDate, endDate);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public Task Update<T>(string objectType, string id, T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var req = RequestBuilder.Update(objectType, id, obj, fields);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public Task UpdateByExternalId<T>(string objectType, Expression<Func<T, object>> externalIdFunc, string externalId, T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var req = RequestBuilder.UpdateByExternalId(objectType, externalIdFunc, externalId, obj, fields);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public Task UpdateByExternalId<T>(string objectType, string externalFieldName, string externalId, T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var req = RequestBuilder.UpdateByExternalId(objectType, externalFieldName, externalId, obj, fields);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public Task<string> Create<T>(string objectType, T obj, FieldsCollection<T> fields) where T : BaseObject
    {
      var req = RequestBuilder.Create(objectType, obj, fields);
      AddRequest(req);
      return req.ResponseTask.Task;
    }

    public async Task<string> PostChatterItem(string objectId, IFeedItem item)
    {
      var req = RequestBuilder.PostChatterItem(objectId, item);
      AddRequest(req);
      return await req.ResponseTask.Task;
    }
  }
}