using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using KDAPILib.SalesForceCloudAPI.Objects;
using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI
{
  public static class ObjectUtils
  {
    public static string GetFieldJsonName<T>(Expression<Func<T, object>> exp)
    {
      MemberInfo GetMemberInfo()
      {
        switch (exp.Body) {
          case MemberExpression me:
            return me.Member;

          case UnaryExpression un:
            switch (un.NodeType) {
              case ExpressionType.Convert:
                return ((MemberExpression)un.Operand).Member;

              default:
                throw new Exception($"unknown operator");
            }

          default:
            throw new Exception($"invalid node");
        }
      }

      var w = (JsonPropertyAttribute)GetMemberInfo().GetCustomAttribute(typeof(JsonPropertyAttribute), true);
      return w.PropertyName;
    }

    public static IList<string> GetAllFields(Type objectType)
    {
      (MemberInfo, object[]) GetJsonAttr(MemberInfo p) => (p, p.GetCustomAttributes(typeof(JsonPropertyAttribute), true));

      const BindingFlags PRIVATE_PUBLIC = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
      const BindingFlags ONLY_PRIVATE = BindingFlags.Instance | BindingFlags.NonPublic;

      var chain = Enumerable.Empty<(MemberInfo, object[])>();

      chain = chain.Concat(objectType.GetProperties(PRIVATE_PUBLIC).Select(GetJsonAttr))
                   .Concat(objectType.GetFields(PRIVATE_PUBLIC).Select(GetJsonAttr));

      var type = objectType;
      while ((type = type.BaseType) != null)
        chain = chain.Concat(type.GetProperties(ONLY_PRIVATE).Select(GetJsonAttr))
                     .Concat(type.GetFields(ONLY_PRIVATE).Select(GetJsonAttr));

      return chain.Where(x => x.Item2.Length >= 1)
                  .SelectMany(x => {
                    var propName = ((JsonPropertyAttribute)x.Item2[0]).PropertyName;

                    Type itemType = null;
                    if (x.Item1 is PropertyInfo pi)
                      itemType = pi.PropertyType;
                    else if (x.Item1 is FieldInfo fi)
                      itemType = fi.FieldType;

                    if (itemType != null) {
                      if (itemType.GetCustomAttribute<SFValueTypeAttribute>() == null) {
                        var subFields = GetAllFields(itemType);
                        if (subFields.Count > 0)
                          return subFields.Select(y => propName + "." + y);
                      }
                    }

                    return new List<string>() { propName };
                  })
                  .ToList();
    }

    public static IList<string> GetAllFields<T>()
    {
      return GetAllFields(typeof(T));
    }
  }
}