using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using KDAPILib.Builders;
using KDLib;
using Newtonsoft.Json.Linq;
using KDAPILib.SalesForceCloudAPI.API;
using KDAPILib.SalesForceCloudAPI.Objects;
using RequestsNET;

namespace KDAPILib.SalesForceCloudAPI
{
  public class SalesForceCloudAPI
  {
    private readonly string _instance;
    private readonly string _refreshToken;
    private readonly string _customerKey;
    private readonly string _customerSecret;

    private readonly SemaphoreSlim _tokenRefreshSemaphore = new SemaphoreSlim(1);
    private string _token;
    private DateTime _tokenIssuesDate;

    public SalesForceCloudAPI(string instance, string customerKey, string customerSecret, string refreshToken)
    {
      _instance = instance;
      _customerKey = customerKey;
      _customerSecret = customerSecret;
      _refreshToken = refreshToken;
    }

    // GetObject
    public async Task<GetObjectResponse<T>> GetObject<T>(string objectType, string id, DateTime? ifModifiedSince = null) where T : BaseObject
    {
      var compositeRequest = CreateCompositeRequest();
      var req = compositeRequest.GetObject<T>(objectType, id, ifModifiedSince);
      await compositeRequest.Execute();
      return await req;
    }

    public async Task<GetObjectResponse<T>> GetObjectByExternalId<T>(string objectType, Expression<Func<T, object>> externalIdFunc, string externalId, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var compositeRequest = CreateCompositeRequest();
      var req = compositeRequest.GetObjectByExternalId<T>(objectType, externalIdFunc, externalId, fields);
      await compositeRequest.Execute();
      return await req;
    }

    public async Task<GetObjectResponse<T>> GetObjectByExternalId<T>(string objectType, string externalFieldName, string externalId, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var compositeRequest = CreateCompositeRequest();
      var req = compositeRequest.GetObjectByExternalId<T>(objectType, externalFieldName, externalId, fields);
      await compositeRequest.Execute();
      return await req;
    }

    public async Task<List<T>> GetObjects<T>(string objectType, IEnumerable<string> ids) where T : BaseObject
    {
      var idsList = ids.ToList();
      if (!idsList.Any())
        return new List<T>();

      return await AsyncUtils.TransformInChunksAsync<string, T>(
          idsList,
          2000,
          4,
          async chunkIds => {
            Console.WriteLine($"run {chunkIds.Count()}, TH: {Thread.CurrentThread.ManagedThreadId}");
            var t = await DoPost($"services/data/v42.0/composite/sobjects/{objectType}",
                                 body: JToken.FromObject(new {
                                     ids = chunkIds,
                                     fields = ObjectUtils.GetAllFields<T>(),
                                 }));
            if (!t.Success)
              throw Utils.ParseError(t.Json);
            return t.Json.ToObject<List<T>>();
          },
          TaskScheduler.Default);
    }

    public async Task<List<T>> QueryAll<T>(string query) where T : BaseObject
    {
      var response = await DoGet("/services/data/v39.0/queryAll",
                                 parameters: new Dictionary<string, string>() {
                                     ["q"] = query
                                 });

      if (response.StatusCode == HttpStatusCode.OK) {
        var jsonData = response.Json;

        var nextRecordsUrl = (string)jsonData["nextRecordsUrl"];
        var totalSize = jsonData["totalSize"].Value<int>();
        var done = jsonData["done"].Value<bool>();
        var records = jsonData["records"].ToObject<List<T>>();

        while (!done) {
          Console.WriteLine($"{records.Count} of {totalSize}");

          response = await DoGet(nextRecordsUrl);

          if (response.StatusCode == HttpStatusCode.OK) {
            jsonData = response.Json;

            nextRecordsUrl = (string)jsonData["nextRecordsUrl"];
            done = jsonData["done"].Value<bool>();
            records.AddRange(jsonData["records"].ToObject<List<T>>());
          }
          else {
            throw new Exception("request failed");
          }
        }

        Trace.Assert(done == true);
        Trace.Assert(totalSize == records.Count);

        return records;
      }
      else {
        throw new Exception("request failed");
      }
    }

    public async Task<GetChangesResponse> GetChanges(string objectType, DateTime startDate, DateTime endDate)
    {
      var compositeRequest = CreateCompositeRequest();
      var req = compositeRequest.GetChanges(objectType, startDate, endDate);
      await compositeRequest.Execute();
      return await req;
    }

    public async Task<GetDeletedResponse> GetDeleted(string objectType, DateTime startDate, DateTime endDate)
    {
      var compositeRequest = CreateCompositeRequest();
      var req = compositeRequest.GetDeleted(objectType, startDate, endDate);
      await compositeRequest.Execute();
      return await req;
    }

    public Task Update<T>(string id, T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      return Update(obj.ObjectName, id, obj, fields);
    }

    public async Task Update<T>(string objectType, string id, T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var compositeRequest = CreateCompositeRequest();
      var req = compositeRequest.Update(objectType, id, obj, fields);
      await compositeRequest.Execute();
      await req;
    }

    public async Task UpdateByExternalId<T>(string objectType, Expression<Func<T, object>> externalIdFunc, string externalId, T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var compositeRequest = CreateCompositeRequest();
      var req = compositeRequest.UpdateByExternalId(objectType, externalIdFunc, externalId, obj, fields);
      await compositeRequest.Execute();
      await req;
    }

    public async Task UpdateByExternalId<T>(string objectType, string externalFieldName, string externalId, T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var compositeRequest = CreateCompositeRequest();
      var req = compositeRequest.UpdateByExternalId(objectType, externalFieldName, externalId, obj, fields);
      await compositeRequest.Execute();
      await req;
    }

    public Task<string> Create<T>(T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      return Create(obj.ObjectName, obj, fields);
    }

    public async Task<string> Create<T>(string objectType, T obj, FieldsCollection<T> fields = null) where T : BaseObject
    {
      var compositeRequest = CreateCompositeRequest();
      var req = compositeRequest.Create(objectType, obj, fields);
      await compositeRequest.Execute();
      return await req;
    }

    // public async Task GetObjectListView(string objectType, string id)
    // {
    //   var res = await DoGet($"services/data/v39.0/sobjects/{objectType}/listviews/{id}/results");
    //   var data = res.ToObject<OpportunityListViewResponse>();
    // }

    public async Task<string> PostChatterItem(string objectId, IFeedItem item)
    {
      var compositeRequest = CreateCompositeRequest();
      var req = compositeRequest.PostChatterItem(objectId, item);
      await compositeRequest.Execute();
      return await req;
    }

    // Internal
    private async Task ExecuteComposite(CompositeRequest b)
    {
      await b.Execute();
    }

    public async Task<Response> DoGet(string endpoint,
                                      Dictionary<string, string> parameters = null,
                                      Dictionary<string, string> headers = null)
    {
      if (_token == null)
        await DoRefreshToken();

      retry:
      var res = await Requests.GetAsync($"https://{_instance}.salesforce.com/{endpoint}",
                                        parameters: parameters,
                                        headers: headers,
                                        authBearerToken: _token /*,
                                   options: new Requests.RequestOptions() {
                                       // LoggingEnabled = true,
                                       // AlwaysLogFailures = true,
                                   }*/);

      if (res.StatusCode == HttpStatusCode.Unauthorized) {
        // var message = (string) res.Json[0]["message"];
        var errorCode = (string)res.Json[0]["errorCode"];

        if (errorCode == "INVALID_SESSION_ID") {
          await DoRefreshToken();
          goto retry;
        }
      }

      return res;
    }

    public async Task<Response> DoPost(string endpoint,
                                       JToken body,
                                       Dictionary<string, string> parameters = null,
                                       Dictionary<string, string> headers = null)
    {
      if (_token == null)
        await DoRefreshToken();

      retry:
      var res = await Requests.PostAsync($"https://{_instance}.salesforce.com/{endpoint}",
                                         json: body,
                                         parameters: parameters,
                                         headers: headers,
                                         authBearerToken: _token);

      if (res.StatusCode == HttpStatusCode.Unauthorized) {
        // var message = (string) res.Json[0]["message"];
        var errorCode = (string)res.Json[0]["errorCode"];

        if (errorCode == "INVALID_SESSION_ID") {
          await DoRefreshToken();
          goto retry;
        }
      }

      return res;
    }


    private async Task DoRefreshToken()
    {
      await _tokenRefreshSemaphore.WaitAsync();
      try {
        if ((DateTime.UtcNow - _tokenIssuesDate).TotalSeconds < 60)
          return;

        var res = await Requests.PostAsync($"https://login.salesforce.com/services/oauth2/token",
                                           parameters: new Dictionary<string, string>() {
                                               ["refresh_token"] = _refreshToken,
                                               ["grant_type"] = "refresh_token",
                                               ["client_id"] = _customerKey,
                                               ["client_secret"] = _customerSecret,
                                               ["format"] = "json",
                                           });

        _token = (string)res.Json["access_token"];
        _tokenIssuesDate = DateTime.UtcNow;
      }
      finally {
        _tokenRefreshSemaphore.Release();
      }
    }


    public CompositeRequest CreateCompositeRequest()
    {
      return new CompositeRequest(this);
    }
  }
}