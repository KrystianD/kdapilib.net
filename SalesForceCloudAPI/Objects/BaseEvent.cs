using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.Objects
{
  public class BaseEvent : BaseObject
  {
    public override string ObjectName { get; } = "Event";

    [JsonProperty("IsDeleted")]
    public bool IsDeleted;

    [JsonProperty("WhatId")]
    public string WhatId;
  }
}