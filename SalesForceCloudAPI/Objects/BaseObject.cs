using System;
using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.Objects
{
  public abstract class BaseObject
  {
    public abstract string ObjectName { get; }

    [JsonProperty("Id")]
    public string Id { get; set; }

    [JsonProperty("SystemModstamp")]
    [JsonConverter(typeof(SFDateTimeConverter))]
    public DateTime SystemModstamp;
  }
}