using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.Objects
{
  public class Account : BaseObject
  {
    public override string ObjectName { get; } = "Account";

    [JsonProperty("Name")]
    public string Name { get; set; }

    [JsonProperty("BillingStreet")]
    public string BillingStreet { get; set; }

    [JsonProperty("BillingCity")]
    public string BillingCity { get; set; }

    [JsonProperty("BillingState")]
    public string BillingState { get; set; }

    [JsonProperty("BillingPostalCode")]
    public string BillingPostalCode { get; set; }

    [JsonProperty("BillingCountry")]
    public string BillingCountry { get; set; }

    [JsonProperty("Phone")]
    public string Phone { get; set; }
  }
}