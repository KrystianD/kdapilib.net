using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.Objects
{
  public class BaseUser : BaseObject
  {
    public override string ObjectName { get; } = "User";

    [JsonProperty("Name")]
    public string Name { get; set; }

    [JsonProperty("Email")]
    public string Email { get; set; }
  }
}