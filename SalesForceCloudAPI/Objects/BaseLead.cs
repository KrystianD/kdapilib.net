using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.Objects
{
  public class BaseLead : BaseObject
  {
    public override string ObjectName { get; } = "Lead";

    [JsonProperty("LastName")]
    public string LastName { get; set; }

    [JsonProperty("Company")]
    public string Company { get; set; }

    [JsonProperty("Status")]
    public string Status { get; set; }

    [JsonProperty("Phone")]
    public string Phone { get; set; }

    [JsonProperty("Email")]
    public string Email { get; set; }

    [JsonProperty("Description")]
    public string Description { get; set; }

    [JsonProperty("IVR_selection__c")]
    public string IVRSelection { get; set; }

    [JsonProperty("LeadSource")]
    public string LeadSource { get; set; }

    [JsonProperty("RecordTypeId")]
    public string RecordTypeId { get; set; }
  }
}