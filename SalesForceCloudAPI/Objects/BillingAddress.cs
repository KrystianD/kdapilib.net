using System;
using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.Objects
{
  [SFValueType]
  public class BillingAddress
  {
    [JsonProperty("city")]
    public string City { get; set; }

    [JsonProperty("country")]
    public string Country { get; set; }

    // [JsonProperty("geocodeAccuracy")]
    // public string GeocodeAccuracy{ get; set; }

    // [JsonProperty("latitude")]
    // public string Latitude{ get; set; }
    //
    // [JsonProperty("longitude")]
    // public string Longitude{ get; set; }

    [JsonProperty("postalCode")]
    public string PostalCode { get; set; }

    [JsonProperty("state")]
    public string State { get; set; }

    [JsonProperty("street")]
    public string Street { get; set; }
  }

  public class SFValueTypeAttribute : Attribute { }
}