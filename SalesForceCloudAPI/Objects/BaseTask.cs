using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.Objects
{
  public class BaseTask : BaseObject
  {
    public override string ObjectName { get; } = "Task";

    [JsonProperty("IsDeleted")]
    public bool IsDeleted;
    
    [JsonProperty("WhatId")]
    public string WhatId;
  }
}