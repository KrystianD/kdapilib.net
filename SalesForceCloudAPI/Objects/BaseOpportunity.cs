using System;
using Newtonsoft.Json;

namespace KDAPILib.SalesForceCloudAPI.Objects
{
  public class BaseOpportunity : BaseObject
  {
    public override string ObjectName { get; } = "Opportunity";

    [JsonProperty("IsDeleted")]
    public bool IsDeleted;
    
    [JsonProperty("Name")]
    public string Name { get; set; }

    [JsonProperty("AccountId")]
    public string AccountId { get; set; }

    [JsonProperty("OwnerId")]
    public string OwnerId { get; set; }

    [JsonProperty("Description")]
    public string Description;

    [JsonProperty("StageName")]
    public string StageName;

    [JsonProperty("Amount")]
    public decimal? Amount;

    [JsonProperty("CloseDate")]
    [JsonConverter(typeof(SFDateConverter))]
    public DateTime CloseDate;

    [JsonProperty("LeadSource")]
    public string LeadSource;

    [JsonProperty("IsClosed")]
    public bool? IsClosed;

    [JsonProperty("IsWon")]
    public bool? IsWon;

    [JsonProperty("CreatedDate")]
    [JsonConverter(typeof(SFDateTimeConverter))]
    public DateTime CreatedDate;

    [JsonProperty("LastModifiedDate")]
    [JsonConverter(typeof(SFDateTimeConverter))]
    public DateTime LastModifiedDate;

    [JsonProperty("Probability")]
    public int Probability;
    
    [JsonProperty("LastActivityDate")]
    [JsonConverter(typeof(SFDateConverter))]
    public DateTime? LastActivityDate;
  }
}