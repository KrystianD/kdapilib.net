﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using KDLib;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RequestsNET;

namespace KDAPILib.SendInBlue
{
  namespace Models
  {
    public class Contact
    {
      [JsonProperty("email", Required = Required.Always)]
      public string Email;

      [JsonProperty("id", Required = Required.Always)]
      public int Id;

      [JsonProperty("listIds", Required = Required.Always)]
      public List<int> ListIds;

      [JsonProperty("attributes", Required = Required.DisallowNull)]
      public Dictionary<string, object> Attributes = new Dictionary<string, object>();

      public string GetAttributeString(string oid)
      {
        if (Attributes.TryGetValue(oid, out var value))
          return (string)value;
        return null;
      }
    }

    public class CreateContactModel
    {
      [JsonProperty("email")]
      public string Email;

      [JsonProperty("listIds")]
      public List<int> ListIds;

      [JsonProperty("attributes")]
      public Dictionary<string, object> Attributes;
    }

    public class UpdateContactModel
    {
      [JsonProperty("attributes")]
      public Dictionary<string, object> Attributes;

      [JsonProperty("listIds")]
      public List<int> ListIds;

      [JsonProperty("unlinkListIds")]
      public List<int> UnlinkListIds;
    }
  }

  public class SendInBlueAPI
  {
    private readonly string _key;

    public SendInBlueAPI(string key)
    {
      _key = key;
    }

    public string OverrideRecipient { get; set; }

    public Builder CreateBuilder(int templateId)
    {
      return new Builder(this, templateId);
    }

    // public async Task<JToken> GetReport()
    // {
    //   var resp = await Requests.Get(
    //       "https://api.sendinblue.com/v3/smtp/statistics/events",
    //       parameters: new Dictionary<string, string>() {
    //           { "event", "opened" },
    //           { "messageId", "<201804202250.46267048222@smtp-relay.sendinblue.com>" },
    //       },
    //       headers: new Dictionary<string, string> {
    //           { "api-key", _key }
    //       });
    //
    //   return resp.Json;
    // }

    public async Task<List<Models.Contact>> GetAllContacts()
    {
      var contacts = new List<Models.Contact>();

      do {
        var resp = await PerformGetRequest(
            "/contacts", new Dictionary<string, string>() {
                ["offset"] = contacts.Count.ToString(),
                ["limit"] = "500",
            });

        ValidateResponse(resp);

        var respJson = JToken.Parse(resp.ParseAsText());

        var currentContacts = respJson["contacts"].ToObject<List<Models.Contact>>();
        contacts.Capacity = respJson["count"].Value<int>();

        contacts.AddRange(currentContacts);
      } while (contacts.Count < contacts.Capacity);

      return contacts;
    }

    public async Task<List<Models.Contact>> GetContactsInList(int listId)
    {
      var contacts = new List<Models.Contact>();

      do {
        var resp = await PerformGetRequest(
            $"/contacts/lists/{listId}/contacts",
            new Dictionary<string, string>() {
                ["offset"] = contacts.Count.ToString(),
                ["limit"] = "500",
            });

        ValidateResponse(resp);

        var currentContacts = resp.Json["contacts"].ToObject<List<Models.Contact>>();
        contacts.Capacity = resp.Json["count"].Value<int>();

        contacts.AddRange(currentContacts);
      } while (contacts.Count < contacts.Capacity);

      return contacts;
    }

    public async Task<Models.Contact> GetContactByEmail(string email)
    {
      var resp = await PerformGetRequest($"/contacts/{email}");

      ValidateResponse(resp);

      return resp.Json.ToObject<Models.Contact>();
    }

    public async Task<int> CreateContact(Models.CreateContactModel contact)
    {
      var data = JToken.FromObject(contact);

      var resp = await PerformPostRequest("/contacts", data);

      if (resp.StatusCode == HttpStatusCode.BadRequest) {
        var code = resp.Json["code"].Value<string>();
        // var message = resp.Json["message"].Value<string>();

        if (code == "duplicate_parameter")
          throw new ContactAlreadyExistException();
      }

      ValidateResponse(resp);

      return resp.Json["id"].Value<int>();
    }

    public async Task UpdateContact(string email, Models.UpdateContactModel contact)
    {
      var data = JToken.FromObject(contact);

      var resp = await PerformPutRequest($"/contacts/{email}", data);

      if (resp.StatusCode == HttpStatusCode.BadRequest) {
        var code = resp.Json["code"].Value<string>();
        // var message = resp.Json["message"].Value<string>();

        if (code == "document_not_found")
          throw new ContactNotFoundException();
      }

      ValidateResponse(resp);
    }

    public async Task AddExistingContactsToList(int listId, IEnumerable<string> emails)
    {
      var data = JToken.FromObject(new {
          emails = emails,
      });

      var resp = await PerformPostRequest($"/contacts/lists/{listId}/contacts/add", data);

      if (resp.StatusCode == HttpStatusCode.BadRequest) {
        var code = resp.Json["code"].Value<string>();
        // var message = resp.Json["message"].Value<string>();

        if (code == "invalid_parameter" /* && message == "Contact already in list and/or does not exist"*/)
          throw new ContactAlreadyExistException();
      }

      ValidateResponse(resp);
    }

    public async Task RemoveExistingContactsFromList(int listId, IEnumerable<string> emails = null)
    {
      var data = JToken.FromObject(new {
          emails = emails,
          all = emails == null
      });

      var resp = await PerformPostRequest($"/contacts/lists/{listId}/contacts/remove", data);

      if (resp.StatusCode.NotIn(HttpStatusCode.OK, HttpStatusCode.Created))
        throw new UnknownErrorException();
    }

    // Builder
    public class EmailAddress
    {
      [JsonProperty("email")]
      public string email;

      [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
      public string name;

      public EmailAddress(string email, string name = null)
      {
        this.email = email;
        this.name = name;
      }
    }

    public class Builder
    {
      private SendInBlueAPI api;

      private List<EmailAddress> to = new List<EmailAddress>(),
                                 cc = new List<EmailAddress>(),
                                 bcc = new List<EmailAddress>();

      private int templateId;
      private EmailAddress replyTo;
      private Dictionary<string, string> parameters = new Dictionary<string, string>();
      private List<JToken> attachments = new List<JToken>();
      private List<string> tags = new List<string>();

      public Builder(SendInBlueAPI api, int templateId)
      {
        this.api = api;
        this.templateId = templateId;
      }

      public Builder AddTag(string tag)
      {
        tags.Add(tag);
        return this;
      }

      public Builder AddTo(string email)
      {
        to.Add(new EmailAddress(email));
        return this;
      }

      public Builder AddCc(string email)
      {
        cc.Add(new EmailAddress(email));
        return this;
      }

      public Builder AddBcc(string email)
      {
        bcc.Add(new EmailAddress(email));
        return this;
      }

      public Builder SetReplyTo(string email)
      {
        replyTo = new EmailAddress(email);
        return this;
      }

      public Builder AddParameter(string name, string value)
      {
        parameters.Add(name, value);
        return this;
      }

      public Builder AddAttachment(string fileName, byte[] data)
      {
        var o = new JObject();
        o["name"] = fileName;
        o["content"] = Convert.ToBase64String(data);
        attachments.Add(o);
        return this;
      }

      public Builder AddAttachmentUrl(string fileName, string url)
      {
        var o = new JObject();
        o["name"] = fileName;
        o["url"] = url;
        attachments.Add(o);
        return this;
      }

      public async Task<string> Send()
      {
        if (api.OverrideRecipient != null) {
          to.Clear();
          cc.Clear();
          bcc.Clear();
          to.Add(new EmailAddress(api.OverrideRecipient));
          replyTo = null;
        }

        var json = new JObject {
            { "to", JArray.FromObject(to) },
            { "params", JObject.FromObject(parameters) },
            { "templateId", templateId },
        };

        if (attachments.Count > 0)
          json["attachment"] = JArray.FromObject(attachments);

        if (cc.Count > 0)
          json.Add("cc", JArray.FromObject(cc));
        if (bcc.Count > 0)
          json.Add("bcc", JArray.FromObject(bcc));
        if (replyTo != null)
          json.Add("replyTo", JToken.FromObject(replyTo));

        if (tags.Count > 0)
          json.Add("tags", JArray.FromObject(tags));

        var resp = await Requests.PostAsync(
            "https://api.sendinblue.com/v3/smtp/email",
            json: json,
            headers: new Dictionary<string, string> {
                { "api-key", api._key }
            },
            timeout: TimeSpan.FromSeconds(100));

        resp.ValidateResponse();
        
        if (!(resp.Json is JObject data))
          throw new Exception("Invalid response");

        var messageId = data.GetValue("messageId");
        if (messageId == null) {
          var v = data.GetValue("message");
          if (v == null) {
            throw new Exception("Unable to send message");
          }
          else {
            string message = v.Value<string>();
            throw new Exception(message);
          }
        }
        else {
          return messageId.Value<string>();
        }
      }
    }

    // Helpers
    private Task<Response> PerformGetRequest(string url, Dictionary<string, string> parameters = null)
    {
      return Requests.GetAsync(
          "https://api.sendinblue.com/v3" + url,
          parameters: parameters,
          headers: new Dictionary<string, string> {
              { "api-key", _key }
          },
          timeout: TimeSpan.FromSeconds(100));
    }

    private Task<Response> PerformPostRequest(string url, JToken data)
    {
      return Requests.PostAsync(
          "https://api.sendinblue.com/v3" + url,
          json: data,
          headers: new Dictionary<string, string> {
              { "api-key", _key }
          },
          timeout: TimeSpan.FromSeconds(100));
    }

    private Task<Response> PerformPutRequest(string url, JToken data)
    {
      return Requests.PutAsync(
          "https://api.sendinblue.com/v3" + url,
          json: data,
          headers: new Dictionary<string, string> {
              { "api-key", _key }
          },
          timeout: TimeSpan.FromSeconds(100));
    }

    private void ValidateResponse(Response resp)
    {
      if (resp.StatusCode.NotIn(HttpStatusCode.OK, HttpStatusCode.Created, HttpStatusCode.NoContent))
        throw new UnknownErrorException();
    }
  }

  public class ContactNotFoundException : Exception
  {
  }

  public class ContactAlreadyExistException : Exception
  {
  }

  public class UnknownErrorException : Exception
  {
  }
}