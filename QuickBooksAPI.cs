﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Security;
using Task = System.Threading.Tasks.Task;

namespace KDAPILib.QuickBooks
{
  public class QuickBooksAPI
  {
    private readonly string _baseUrl;

    private readonly string _realmId;

    private readonly OAuth2RequestValidator _oauthValidator;

    public QuickBooksAPI(string baseUrl, string realmId, string accessToken)
    {
      _baseUrl = baseUrl;
      _realmId = realmId;

      _oauthValidator = new OAuth2RequestValidator(accessToken);
    }

    // Customers
    public Task CreateCustomer(Customer customer)
    {
      return CreateObject(customer);
    }

    public Task<Customer> FindCustomerByEmail(string email)
    {
      return QuerySingle<Customer>($"SELECT * FROM Customer WHERE PrimaryEmailAddr='{EscapeQuery(email)}'");
    }

    public Task<Customer> FindRootCustomerByEmail(string email)
    {
      return QuerySingle<Customer>($"SELECT * FROM Customer WHERE PrimaryEmailAddr='{EscapeQuery(email)}' AND Job = false");
    }

    public Task<ReadOnlyCollection<Customer>> FindCustomersByEmail(string email)
    {
      return QueryAll<Customer>($"SELECT * FROM Customer WHERE PrimaryEmailAddr='{EscapeQuery(email)}'");
    }

    public async Task<ReadOnlyCollection<Customer>> GetAllCustomers()
    {
      return await Paginate<Customer>("SELECT * FROM Customer", 4000);
    }

    // Estimates
    public Task<Estimate> CreateEstimate(Estimate estimate)
    {
      return CreateObject(estimate);
    }

    public Task<Estimate> GetEstimate(int id)
    {
      return QuerySingle<Estimate>($"SELECT * FROM Estimate WHERE Id='{id}'");
    }

    public Task<Estimate> GetEstimateByDocNumber(string docNumber)
    {
      return QuerySingle<Estimate>($"SELECT * FROM Estimate WHERE DocNumber='{docNumber}'");
    }

    // Invoices
    public Task<Invoice> CreateInvoice(Invoice invoice)
    {
      return CreateObject(invoice);
    }

    public async Task<ReadOnlyCollection<Invoice>> GetAllInvoices()
    {
      return await Paginate<Invoice>("SELECT * FROM Invoice", 1000);
    }

    public Task<Invoice> GetInvoiceByDocNumber(string docNumber)
    {
      return QuerySingle<Invoice>($"SELECT * FROM Invoice WHERE DocNumber='{docNumber}'");
    }

    // Items
    public async Task<ReadOnlyCollection<Item>> GetAllItems()
    {
      return await Paginate<Item>("SELECT * FROM Item", 1000);
    }

    // Queries
    public async Task<T> QuerySingle<T>(string query, TimeSpan? timeout = null)
    {
      var results = await QueryAll<T>(query, timeout);
      return results.FirstOrDefault();
    }

    public Task<ReadOnlyCollection<T>> QueryAll<T>(string query, TimeSpan? timeout = null)
    {
      return Task.Run(() => {
        try {
          return CreateQueryService<T>(timeout).ExecuteIdsQuery(query);
        }
        catch (Exception e) {
          if (e.InnerException != null && e.InnerException.Message == "The Response Stream was null or empty.")
            throw new TimeoutException();
          throw;
        }
      });
    }

    public Task<List<T>> CDC<T>(DateTime startDate) where T : IEntity, new()
    {
      return Task.Run(() => {
        try {
          var q = CreateDataService().CDC(new List<IEntity>() { new T() }, startDate);
          if (q.exceptions.Count > 0)
            throw new Exception("QB exception");

          if (q.entities.TryGetValue(typeof(T).Name, out var entities))
            return entities.Cast<T>().ToList();
          else
            return new List<T>();
        }
        catch (Exception e) {
          if (e.InnerException != null && e.InnerException.Message == "The Response Stream was null or empty.")
            throw new TimeoutException();
          throw;
        }
      });
    }

    private Task<T> CreateObject<T>(T obj) where T : IEntity
    {
      return Task.Run(() => {
        try {
          return CreateDataService().Add(obj);
        }
        catch (Exception e) {
          if (e.InnerException != null && e.InnerException.Message == "The Response Stream was null or empty.")
            throw new TimeoutException();
          throw;
        }
      });
    }

    // Internal
    private DataService CreateDataService(TimeSpan? timeout = null)
    {
      ServiceContext serviceContext = new ServiceContext(_realmId, IntuitServicesType.QBO, _oauthValidator);
      serviceContext.IppConfiguration.BaseUrl.Qbo = _baseUrl;
      serviceContext.IppConfiguration.MinorVersion.Qbo = "4";
      serviceContext.Timeout = (int)timeout.GetValueOrDefault(TimeSpan.FromSeconds(20)).TotalMilliseconds;
      return new DataService(serviceContext);
    }

    private QueryService<T> CreateQueryService<T>(TimeSpan? timeout = null)
    {
      ServiceContext serviceContext = new ServiceContext(_realmId, IntuitServicesType.QBO, _oauthValidator);
      serviceContext.IppConfiguration.BaseUrl.Qbo = _baseUrl;
      serviceContext.IppConfiguration.MinorVersion.Qbo = "4";
      serviceContext.Timeout = (int)timeout.GetValueOrDefault(TimeSpan.FromSeconds(20)).TotalMilliseconds;
      return new QueryService<T>(serviceContext);
    }

    private string EscapeQuery(string txt)
    {
      return txt.Replace("'", "\\'");
    }

    private async Task<ReadOnlyCollection<T>> PaginateFastBatch<T>(string query, int start, int count)
    {
      const int MAX_PER_REQUEST = 1000;

      int perRequest = Math.Min(MAX_PER_REQUEST, count);

      var tasks = new List<Task<ReadOnlyCollection<T>>>();

      for (int i = 0; i < (count + perRequest - 1) / perRequest; i++) {
        var start2 = start + i * perRequest;
        var q = query + $" STARTPOSITION {start2 + 1} MAXRESULTS {perRequest}";
        Console.WriteLine(q);
        tasks.Add(QueryAll<T>(q));
      }

      var results = await Task.WhenAll(tasks);

      return results.SelectMany(x => x).ToList().AsReadOnly();
    }

    private async Task<ReadOnlyCollection<T>> Paginate<T>(string query, int batchSize)
    {
      var start = 0;
      int lastFetchCount;

      var items = new List<T>();
      do {
        var newItems = await PaginateFastBatch<T>(query, start, batchSize);
        lastFetchCount = newItems.Count;
        start += lastFetchCount;

        items.AddRange(newItems);
      } while (lastFetchCount == batchSize);

      return items.AsReadOnly();
    }
  }

  public class CustomersCollections
  {
    // private List<Customer> _customers;
    private Dictionary<int, Customer> _customerById;
    private DateTime _lastFetchDate;

    public async Task FetchInitial(QuickBooksAPI api)
    {
      _lastFetchDate = DateTime.UtcNow;
      var _customers = new List<Customer>(await api.GetAllCustomers());

      _customerById = new Dictionary<int, Customer>();
      _customers.ForEach(x => _customerById.Add(int.Parse(x.Id), x));
    }

    public TimeSpan DataAge => DateTime.UtcNow - _lastFetchDate;

    public async Task Refresh(QuickBooksAPI api)
    {
      _lastFetchDate = DateTime.UtcNow;
      Console.WriteLine($"checking...");
      var changedEntries = await api.CDC<Customer>(_lastFetchDate - TimeSpan.FromMinutes(10));

      foreach (var entry in changedEntries) {
        var id = int.Parse(entry.Id);
        if (_customerById.TryGetValue(id, out var existingCustomer)) {
          Console.WriteLine($"updating entry {id}");
        }
        else {
          Console.WriteLine($"new entry {id}");
        }

        _customerById[id] = entry;
      }
    }

    public IEnumerable<Customer> Customers => _customerById.Values;

    public IEnumerable<Customer> RootCustomers => Customers.Where(x => x.Level == 0);
    public IEnumerable<Customer> Jobs => Customers.Where(x => x.Level == 1);

    public Customer FindRootCustomerByEmail(string email)
    {
      return RootCustomers.FirstOrDefault(x => x.Active && x.PrimaryEmailAddr != null && x.PrimaryEmailAddr.Address.ToLower() == email.ToLower());
    }

    public Customer FindRootCustomerByDisplayName(string displayName)
    {
      return RootCustomers.FirstOrDefault(x => x.Active && string.Equals(x.DisplayName, displayName, StringComparison.CurrentCultureIgnoreCase));
    }

    public Customer FindJobByDisplayName(string displayName)
    {
      return Jobs.FirstOrDefault(x => string.Equals(x.DisplayName, displayName, StringComparison.CurrentCultureIgnoreCase));
    }
  }
}