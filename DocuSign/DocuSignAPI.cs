﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using KDLib;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RequestsNET;
using DS = DocuSign;

namespace KDAPILib.DocuSign
{
  public class DocuSignEnvelopeVoidedException : Exception
  {
  }

  public class DocuSignEnvelopeNotFoundException : Exception
  {
  }

  public class DocuSignEnvelope
  {
  }

  public class DocuSignAPI
  {
    private string _apiUrl;
    private readonly string _apiUsername;
    private readonly string _apiPassword;
    private readonly string _apiIntegratorKey;

    public DocuSignAPI(string apiUrl, string apiUsername, string apiPassword, string apiIntegratorKey)
    {
      _apiUrl = apiUrl;
      _apiUsername = apiUsername;
      _apiPassword = apiPassword;
      _apiIntegratorKey = apiIntegratorKey;
    }

    public async Task Login()
    {
      var resp = await DoRequest("/v2/login_information");
      _apiUrl = resp.SelectToken("loginAccounts[0].baseUrl").Value<string>();
    }

    public async Task<JObject> GetEnvelopeRecipients(string id)
    {
      var resp = await DoRequest(string.Format("/envelopes/{0}/recipients", id));
      return resp;
    }

    public async Task<JObject> GetEnvelopeRecipients2(string id)
    {
      var resp = await DoRequest(string.Format("/envelopes/{0}/recipients", id));

      var y = resp.ToObject<DS.eSign.Model.Recipients>();

      return resp;
    }

    public async Task<JObject> GetEnvelopeRecipientByEmail(string id, string email)
    {
      var resp = await GetEnvelopeRecipients(id);
      var signers = resp.SelectToken("signers");

      return signers.Where(signer => signer["email"].Value<string>() == email)
                    .Cast<JObject>()
                    .FirstOrDefault();
    }

    // public async Task<JObject> CreateEnvelope(string id, string email)
    // {
    //   JObject data;
    //   var resp = await DoRequest(string.Format("/envelopes/{0}/recipients", id), data, "post", TimeSpan.FromMinutes(2));
    //   
    // }

    public async Task<JObject> GetEnvelopeDocument(string envelopeId, int documentId)
    {
      var resp = await DoRequest(string.Format($"/envelopes/{envelopeId}/documents/{documentId}"));

      var y = resp.ToObject<DS.eSign.Model.Recipients>();

      return resp;
    }

    public async Task<string> CreateSigningLink(string id, string client_user_id, string recipient_id, string user_id,
                                                string ping_url = null, int ping_frequenecy = 300, string return_url = null)
    {
      var data = new Dictionary<string, string>() {
          { "authenticationMethod", "email" },
          { "clientUserId", client_user_id },
          { "userName", client_user_id },
          { "email", client_user_id },
          { "returnUrl", "https://designeverest.com" },
          { "recipientId", recipient_id },
          { "userId", user_id },
      };

      if (ping_url != null) {
        Debug.Assert(60 <= ping_frequenecy && ping_frequenecy <= 1200);

        data["pingUrl"] = ping_url;
        data["pingFrequency"] = ping_frequenecy.ToString();
      }

      if (return_url != null) {
        data["returnUrl"] = return_url;
      }

      Console.WriteLine("id {0}", id);
      Console.WriteLine("data {0}", data);
      var resp = await DoRequest(string.Format("/envelopes/{0}/views/recipient", id), data: data, method: "post");
      Console.WriteLine("resp {0}", resp);

      return resp["url"].Value<string>();
    }

    private Task<JObject> DoRequest(string url)
      => DoRequest(url, (JObject) null, "get", -1);

    private Task<JObject> DoRequest(string url, int timeout)
      => DoRequest(url, (JObject) null, "get", timeout);

    private Task<JObject> DoRequest(string url, Dictionary<string, string> data, string method = "get", int timeout = -1)
      => DoRequest(url, JObject.FromObject(data), method, timeout);

    private Task<JObject> DoRequest(string url, JObject data, string method = "get", int timeout = -1)
      => DoRequest(url, data, method, TimeSpan.FromMilliseconds(timeout));

    private async Task<JObject> DoRequest(string url, JObject data, string method = "get", TimeSpan? timeout = null)
    {
      var auth = new {
          Username = _apiUsername,
          Password = _apiPassword,
          IntegratorKey = _apiIntegratorKey,
      };

      var headers = new Dictionary<string, string>() {
          { "Accept", "application/json" },
          { "X-DocuSign-Authentication", JToken.FromObject(auth).ToString(Formatting.None) },
      };

      Response resp;
      switch (method) {
        case "get":
          resp = await Requests.GetAsync(_apiUrl + url, headers: headers, timeout: timeout);
          break;
        case "post":
          resp = await Requests.PostAsync(_apiUrl + url, json: data, headers: headers, timeout: timeout);
          break;
        default:
          throw new Exception("invalid method");
      }

      if (resp.StatusCode == HttpStatusCode.NotFound) {
        throw new DocuSignEnvelopeNotFoundException();
      }

      if (resp.StatusCode == HttpStatusCode.BadRequest) {
        string errorCode = resp.Json["errorCode"].Value<string>();
        if (errorCode == "ENVELOPE_HAS_BEEN_VOIDED")
          throw new DocuSignEnvelopeVoidedException();
        else
          throw new Exception($"invalid DocuSign response: {resp.Json}");
      }

      if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.Created) {
        throw new Exception($"invalid DocuSign response: {resp.StatusCode}");
      }

      return (JObject) resp.Json;
    }
  }
}