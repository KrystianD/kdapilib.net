﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DocuSign.eSign.Api;
using DocuSign.eSign.Model;
using KDAPILib.DocuSign.Webhook;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Extensions;
using DS = DocuSign;

namespace KDAPILib.DocuSign.Wrapper
{
  public class EnvelopeDoesNotExistException : Exception { }

  public class DocuSignAPIWrapper
  {
    private readonly string _accountId;
    private readonly DS.eSign.Client.Configuration _config;

    public DocuSignAPIWrapper(string apiUrl, string apiUsername, string apiPassword, string apiIntegratorKey, string accountId)
    {
      _accountId = accountId;

      var apiClient = new DS.eSign.Client.ApiClient();

      var auth = new {
          Username = apiUsername,
          Password = apiPassword,
          IntegratorKey = apiIntegratorKey,
      };

      apiClient.RestClient.BaseUrl = new Uri(apiUrl);
      apiClient.RestClient.AddDefaultHeader("X-DocuSign-Authentication", JToken.FromObject(auth).ToString(Formatting.None));

      _config = new DS.eSign.Client.Configuration(apiClient);
    }

    public async Task<DSResponse<DS.eSign.Model.Envelope>> GetEnvelope(string id)
    {
      var api = new DS.eSign.Api.EnvelopesApi(_config);
      try {
        var resp = await api.GetEnvelopeAsyncWithHttpInfo(_accountId, id, new DS.eSign.Api.EnvelopesApi.GetEnvelopeOptions() { });
        return CreateResponse(resp);
      }
      catch (DS.eSign.Client.ApiException e) {
        if (e.ErrorCode == 404) {
          throw new EnvelopeDoesNotExistException();
        }
        else {
          throw;
        }
      }
    }

    public class DSResponse<T>
    {
      public T Data;
      public DateTime ServerTime;
    }

    public async Task<byte[]> GetEnvelopeDocument(string id)
    {
      var api = new DS.eSign.Api.EnvelopesApi(_config);
      var resp = await api.GetDocumentAsyncWithHttpInfo(_accountId, id, "1");
      return await ReadFully(resp.Data);
    }

    private static DSResponse<T> CreateResponse<T>(DS.eSign.Client.ApiResponse<T> response)
    {
      string dateStr = response.Headers["Date"];
      var date = DateTime.ParseExact(dateStr,
                                     "ddd, dd MMM yyyy HH:mm:ss 'GMT'",
                                     CultureInfo.InvariantCulture.DateTimeFormat,
                                     DateTimeStyles.AssumeUniversal).ToUniversalTime();

      return new DSResponse<T> {
          Data = response.Data,
          ServerTime = date
      };
    }

    public async Task<DSResponse<DS.eSign.Model.Recipients>> GetEnvelopeRecipients(string id)
    {
      var api = new DS.eSign.Api.EnvelopesApi(_config);
      var resp = await api.ListRecipientsAsyncWithHttpInfo(_accountId, id, new DS.eSign.Api.EnvelopesApi.ListRecipientsOptions() { includeTabs = "true" });
      return CreateResponse(resp);
    }

    public async Task<DS.eSign.Model.EnvelopeSummary> CreateEnvelope(DS.eSign.Model.EnvelopeDefinition envelopeDefinition)
    {
      var api = new DS.eSign.Api.EnvelopesApi(_config);
      return await api.CreateEnvelopeAsync(_accountId, envelopeDefinition);
    }

    public async Task ResendEnvelope(string id)
    {
      var api = new DS.eSign.Api.EnvelopesApi(_config);
      await api.UpdateAsync(_accountId, id, new Envelope(), new EnvelopesApi.UpdateOptions() { resendEnvelope = "true" });
    }

    public async Task VoidEnvelope(string id)
    {
      var api = new DS.eSign.Api.EnvelopesApi(_config);
      await api.UpdateAsync(_accountId, id, new Envelope() { Status = "voided", VoidedReason = "voided" });
    }

    public async Task<string> CreateSigningLink(string id,
                                                string client_user_id, string user_name, string user_email,
                                                string return_url, string ping_url = null, int ping_frequenecy = 300)
    {
      var api = new DS.eSign.Api.EnvelopesApi(_config);

      var req = new DS.eSign.Model.RecipientViewRequest() {
          // RecipientId = recipient_id,
          ClientUserId = client_user_id,
          // UserId = user_id,
          UserName = user_name,
          Email = user_email,
          AuthenticationMethod = "email",
          ReturnUrl = return_url,
      };

      if (ping_url != null) {
        req.PingUrl = ping_url;
        req.PingFrequency = ping_frequenecy.ToString();
      }

      DS.eSign.Model.ViewUrl recipientView = await api.CreateRecipientViewAsync(_accountId, id, req);

      return recipientView.Url;
    }

    public static T Deserialize<T>(JObject json) => Deserialize<T>(json.ToString(Formatting.None));

    public static T Deserialize<T>(string json)
    {
      JsonSerializerSettings serializerSettings = new JsonSerializerSettings() {
          ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
          DateParseHandling = DateParseHandling.DateTime,
          DateFormatHandling = DateFormatHandling.IsoDateFormat,
          DateTimeZoneHandling = DateTimeZoneHandling.Utc,
      };

      return (T)JsonConvert.DeserializeObject(json, typeof(T), serializerSettings);
    }

    public static async Task<byte[]> ReadFully(Stream stream)
    {
      byte[] buffer = new byte[32768];
      using (MemoryStream ms = new MemoryStream()) {
        while (true) {
          int read = await stream.ReadAsync(buffer, 0, buffer.Length);
          if (read <= 0)
            return ms.ToArray();
          ms.Write(buffer, 0, read);
        }
      }
    }
  }

  public class EnvelopeWrapper
  {
    private readonly DS.eSign.Model.Envelope _envelope;

    public DateTime? CreatedDateTime
    {
      get => _envelope.CreatedDateTime == null ? null : (DateTime?)DateTime.Parse(_envelope.CreatedDateTime).ToUniversalTime();
      set => _envelope.CreatedDateTime = value?.ToString("o");
    }

    public DateTime? SentDateTime
    {
      get => _envelope.SentDateTime == null ? null : (DateTime?)DateTime.Parse(_envelope.SentDateTime).ToUniversalTime();
      set => _envelope.SentDateTime = value?.ToString("o");
    }

    public DateTime? DeliveredDateTime
    {
      get => _envelope.DeliveredDateTime == null ? null : (DateTime?)DateTime.Parse(_envelope.DeliveredDateTime).ToUniversalTime();
      set => _envelope.DeliveredDateTime = value?.ToString("o");
    }

    public DateTime? CompletedDateTime
    {
      get => _envelope.CompletedDateTime == null ? null : (DateTime?)DateTime.Parse(_envelope.CompletedDateTime).ToUniversalTime();
      set => _envelope.CompletedDateTime = value?.ToString("o");
    }

    public DateTime? DeclinedDateTime
    {
      get => _envelope.DeclinedDateTime == null ? null : (DateTime?)DateTime.Parse(_envelope.DeclinedDateTime).ToUniversalTime();
      set => _envelope.DeclinedDateTime = value?.ToString("o");
    }

    public DateTime? VoidedDateTime
    {
      get => _envelope.VoidedDateTime == null ? null : (DateTime?)DateTime.Parse(_envelope.VoidedDateTime).ToUniversalTime();
      set => _envelope.VoidedDateTime = value?.ToString("o");
    }

    public DateTime? DeletedDateTime
    {
      get => _envelope.DeletedDateTime == null ? null : (DateTime?)DateTime.Parse(_envelope.DeletedDateTime).ToUniversalTime();
      set => _envelope.DeletedDateTime = value?.ToString("o");
    }

    public DateTime StatusChangedDateTime
    {
      get => DateTime.Parse(_envelope.StatusChangedDateTime).ToUniversalTime();
      set => _envelope.StatusChangedDateTime = value.ToString("o");
    }

    public StatusEnum Status
    {
      get => (StatusEnum)Enum.Parse(typeof(StatusEnum), _envelope.Status, true);
      set => _envelope.Status = value.ToString().ToLower();
    }

    public string VoidedReason
    {
      get => _envelope.VoidedReason;
      set => _envelope.VoidedReason = value;
    }

    public EnvelopeWrapper(DS.eSign.Model.Envelope envelope)
    {
      _envelope = envelope;
    }

    public JToken ToJson() => JToken.Parse(_envelope.ToJson());
  }

  public class RecipientsWrapper
  {
    private readonly DS.eSign.Model.Recipients _recipients;

    public readonly List<RecipientsSignerWrapper> Signers;

    public RecipientsWrapper(DS.eSign.Model.Recipients recipients)
    {
      _recipients = recipients;
      Signers = recipients.Signers.Select(x => new RecipientsSignerWrapper(x)).ToList();
    }

    public JToken ToJson() => JToken.Parse(_recipients.ToJson());

    public Dictionary<string, string> GetTabsValues()
    {
      return Signers.SelectMany(x => x.GetTabsValues()).ToDictionary(x => x.Key, x => x.Value);
    }
  }

  public class RecipientsSignerWrapper
  {
    private readonly DS.eSign.Model.Signer _signer;

    public string RecipientId => _signer.RecipientId;
    public string RecipientIdGuid => _signer.RecipientIdGuid;
    public string ClientUserId => _signer.ClientUserId;

    public DateTime? SentDateTime
    {
      get => _signer.SentDateTime == null ? null : (DateTime?)DateTime.Parse(_signer.SentDateTime).ToUniversalTime();
      set => _signer.SentDateTime = value?.ToString("o");
    }

    public DateTime? DeliveredDateTime
    {
      get => _signer.DeliveredDateTime == null ? null : (DateTime?)DateTime.Parse(_signer.DeliveredDateTime).ToUniversalTime();
      set => _signer.DeliveredDateTime = value?.ToString("o");
    }

    public DateTime? SignedDateTime
    {
      get => _signer.SignedDateTime == null ? null : (DateTime?)DateTime.Parse(_signer.SignedDateTime).ToUniversalTime();
      set => _signer.SignedDateTime = value?.ToString("o");
    }

    public DateTime? DeclinedDateTime
    {
      get => _signer.DeclinedDateTime == null ? null : (DateTime?)DateTime.Parse(_signer.DeclinedDateTime).ToUniversalTime();
      set => _signer.DeclinedDateTime = value?.ToString("o");
    }

    public StatusEnum Status
    {
      get => (StatusEnum)Enum.Parse(typeof(StatusEnum), _signer.Status, true);
      set => _signer.Status = value.ToString().ToLower();
    }

    public string DeclineReason
    {
      get => _signer.DeclinedReason;
      set => _signer.DeclinedReason = value;
    }

    public RecipientsSignerWrapper(DS.eSign.Model.Signer signer)
    {
      _signer = signer;
    }

    public List<KeyValuePair<string, string>> GetTabsValues()
    {
      var values = new List<KeyValuePair<string, string>>();
      if (_signer.Tabs.TextTabs != null)
        values.AddRange(_signer.Tabs.TextTabs.Select(x => new KeyValuePair<string, string>(x.TabLabel, x.Value)));
      if (_signer.Tabs.NoteTabs != null)
        values.AddRange(_signer.Tabs.NoteTabs.Select(x => new KeyValuePair<string, string>(x.TabLabel, x.Value)));
      if (_signer.Tabs.EmailAddressTabs != null)
        values.AddRange(_signer.Tabs.EmailAddressTabs.Select(x => new KeyValuePair<string, string>(x.TabLabel, x.Value)));
      if (_signer.Tabs.FirstNameTabs != null)
        values.AddRange(_signer.Tabs.FirstNameTabs.Select(x => new KeyValuePair<string, string>(x.TabLabel, x.Value)));
      if (_signer.Tabs.LastNameTabs != null)
        values.AddRange(_signer.Tabs.LastNameTabs.Select(x => new KeyValuePair<string, string>(x.TabLabel, x.Value)));
      return values;
    }
  }
}