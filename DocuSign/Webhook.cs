using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace KDAPILib.DocuSign.Webhook
{
  public enum StatusEnum
  {
    [XmlEnum(Name = "Created")]
    Created,

    [XmlEnum(Name = "Sent")]
    Sent,

    [XmlEnum(Name = "Delivered")]
    Delivered,

    [XmlEnum(Name = "Declined")]
    Declined,

    [XmlEnum(Name = "Voided")]
    Voided,

    [XmlEnum(Name = "Autoresponded")]
    AutoResponded,

    [XmlEnum(Name = "Faxpending")]
    Faxpending,

    [XmlEnum(Name = "Signed")]
    Signed,

    [XmlEnum(Name = "Completed")]
    Completed,

    [XmlEnum(Name = "Deleted")]
    Deleted,
  }

  [XmlRoot("DocuSignEnvelopeInformation", Namespace = "http://www.docusign.net/API/3.0")]
  public class DocuSignEnvelopeInformation
  {
    [XmlElement("TimeZone")]
    public string TimeZone { get; set; }

    [XmlElement("TimeZoneOffset")]
    public int TimeZoneOffset { get; set; }

    [XmlElement("EnvelopeStatus")]
    public DocuSignEnvelopeStatus EnvelopeStatus { get; set; }
  }

  public class DocuSignEnvelopeStatus
  {
    [XmlElement("TimeGenerated")]
    public DateTime TimeGenerated { get; set; }

    [XmlElement("EnvelopeID")]
    public string EnvelopeID { get; set; }

    [XmlElement("Status")]
    public StatusEnum Status { get; set; }

    [XmlElement("Created")]
    public DateTime Created { get; set; }

    [XmlElement("Sent")]
    public DateTime? Sent { get; set; }

    [XmlElement("Delivered")]
    public DateTime? Delivered { get; set; }

    [XmlElement("Declined")]
    public DateTime? Declined { get; set; }

    [XmlElement("Signed")]
    public DateTime? Signed { get; set; }

    [XmlElement("Completed")]
    public DateTime? Completed { get; set; }

    [XmlArray("RecipientStatuses")]
    [XmlArrayItem("RecipientStatus")]
    public List<DocuSignRecipientStatus> RecipientStatuses { get; set; }

    [XmlElement("VoidReason")]
    public string VoidReason { get; set; }

    public Dictionary<string, string> GetTabsValues()
    {
      return RecipientStatuses.SelectMany(x => x.GetTabsValues()).ToDictionary(x => x.Key, x => x.Value);
    }
  }

  public class DocuSignRecipientStatus
  {
    public enum TypeEnum
    {
      [XmlEnum(Name = "Signer")]
      Signer,

      [XmlEnum(Name = "CarbonCopy")]
      CarbonCopy,
    }

    [XmlElement("Type")]
    public TypeEnum Type { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("Sent")]
    public DateTime? Sent { get; set; }

    [XmlElement("Declined")]
    public DateTime? Declined { get; set; }

    [XmlElement("Delivered")]
    public DateTime? Delivered { get; set; }

    [XmlElement("Signed")]
    public DateTime? Signed { get; set; }

    [XmlElement("Status")]
    public StatusEnum Status { get; set; }

    [XmlElement("DeclineReason", IsNullable = true)]
    public string DeclineReason { get; set; }

    [XmlElement("ClientUserId")]
    public string ClientUserId { get; set; }

    [XmlElement("RecipientId")]
    public string RecipientIdGuid { get; set; }

    [XmlArray("TabStatuses")]
    [XmlArrayItem("TabStatus")]
    public List<TabStatus> TabStatuses { get; set; }

    public List<KeyValuePair<string, string>> GetTabsValues()
    {
      var values = new List<KeyValuePair<string, string>>();
      if (TabStatuses != null)
        values.AddRange(TabStatuses.Where(x => x.TabType == TabStatus.TabTypeEnum.Custom)
                                   .Select(x => new KeyValuePair<string, string>(x.TabLabel, x.TabValue)));
      return values;
    }
  }

  public class TabStatus
  {
    public enum TabTypeEnum
    {
      [XmlEnum(Name = "SignHere")]
      SignHere,

      [XmlEnum(Name = "FullName")]
      FullName,

      [XmlEnum(Name = "DateSigned")]
      DateSigned,

      [XmlEnum(Name = "Custom")]
      Custom,
    }

    [XmlElement("TabType")]
    public TabTypeEnum TabType { get; set; }

    [XmlElement("TabName")]
    public string TabName { get; set; }

    [XmlElement("TabLabel")]
    public string TabLabel { get; set; }

    [XmlElement("TabValue")]
    public string TabValue { get; set; }
  }

  public static class WebhookParser
  {
    public static DocuSignEnvelopeInformation Parse(string payload)
    {
      XmlSerializer serializer = new XmlSerializer(typeof(DocuSignEnvelopeInformation));

      using (var reader = new StringReader(payload)) {
        var inf = (DocuSignEnvelopeInformation)serializer.Deserialize(reader);

        DateTime FixDate(DateTime date) => new DateTimeOffset(date, TimeSpan.FromHours(inf.TimeZoneOffset)).UtcDateTime;

        DateTime? FixDateNullable(DateTime? date)
          => date.HasValue ? (DateTime?)new DateTimeOffset(date.Value, TimeSpan.FromHours(inf.TimeZoneOffset)).UtcDateTime : null;

        inf.EnvelopeStatus.TimeGenerated = FixDate(inf.EnvelopeStatus.TimeGenerated);
        inf.EnvelopeStatus.Created = FixDate(inf.EnvelopeStatus.Created);
        inf.EnvelopeStatus.Sent = FixDateNullable(inf.EnvelopeStatus.Sent);
        inf.EnvelopeStatus.Declined = FixDateNullable(inf.EnvelopeStatus.Declined);
        inf.EnvelopeStatus.Delivered = FixDateNullable(inf.EnvelopeStatus.Delivered);
        inf.EnvelopeStatus.Signed = FixDateNullable(inf.EnvelopeStatus.Signed);
        inf.EnvelopeStatus.Completed = FixDateNullable(inf.EnvelopeStatus.Completed);

        foreach (var x in inf.EnvelopeStatus.RecipientStatuses) {
          x.Sent = FixDateNullable(x.Sent);
          x.Delivered = FixDateNullable(x.Delivered);
          x.Signed = FixDateNullable(x.Signed);
          x.Declined = FixDateNullable(x.Declined);
        }

        return inf;
      }
    }
  }
}